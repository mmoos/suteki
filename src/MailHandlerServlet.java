import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.SendMail;

@SuppressWarnings("serial")
public class MailHandlerServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Properties prop = new Properties();
        Session session = Session.getDefaultInstance(prop, null);
        SendMail sm = new SendMail();
        String br = "\n";
        
        try {
            // 受信メールを取得
            MimeMessage recMsg = new MimeMessage(session, req.getInputStream());
    		
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    		String sentDate = df.format(recMsg.getSentDate());
            String from = recMsg.getFrom().toString();
            String subject = recMsg.getSubject();
            String content = this.getText(recMsg.getContent());
            
            String msgBody = "";
            msgBody += "sent at : " + sentDate + br;
            msgBody += "from : " + from + br;
            msgBody += "subject : " + subject + br;
            msgBody += "-----------content----------" + br + content;
            sm.send("momo@pm.ctt.ne.jp", "", "sutekitest転送", msgBody);
        } catch (MessagingException e) {
        	sm.send("momo@pm.ctt.ne.jp", "", "sutekitestメール受信エラー", e.getMessage()+br+e.getCause());
        }
    }

    private String getText(Object content) throws IOException,
            MessagingException {
        String text = null;
        StringBuffer sb = new StringBuffer();

        if (content instanceof String) {
            sb.append((String) content);
        } else if (content instanceof Multipart) {
            Multipart mp = (Multipart) content;
            for (int i = 0; i < mp.getCount(); i++) {
                BodyPart bp = mp.getBodyPart(i);
                sb.append(getText(bp.getContent()));
            }
        }

        text = sb.toString();
        return text;
    }
}