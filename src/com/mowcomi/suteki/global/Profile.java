package com.mowcomi.suteki.global;

import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.data.Member;
import java.util.HashMap;

public class Profile {
	private Member classMember = new Member();
	private HashMap<String,String> memberData = new HashMap<String,String>();
	public Profile(){}
	
	public void setMemberData(String memberID){
		String[] memberRowColumns = classMember.getNewRow().getColumns();
		if(memberID!=null && !memberID.isEmpty()){
			Row memberRow = this.classMember.getMemberByID(memberID);
			for(int i=0; i<memberRowColumns.length; i++){
				String column = memberRowColumns[i];
				this.memberData.put(column, memberRow.get(column));
			}
		}
	}
	
	public String getProfileInput(String column){
		Member classMember = new Member();
		String str = "";
		String value = this.memberData.get(column)==null ? "" : this.memberData.get(column);
		
		String showEmailChk = "";
		showEmailChk += "<div>";
		showEmailChk += "<input type='checkbox' onClick='if(jQuery(this).attr(\"checked\")){jQuery(\"#show_email\").val(\""+classMember.ALWAYS_SHOW+"\");}else{jQuery(\"#show_email\").val(\""+classMember.NEVER_SHOW+"\");}'";
		if(this.memberData.get("show_email")!=null && this.memberData.get("show_email").equals(classMember.ALWAYS_SHOW)){ str += " checked"; }
		showEmailChk +=	">";
		String showEmail = this.memberData.get("show_email")==null ? classMember.NEVER_SHOW : this.memberData.get("show_email");
		showEmailChk += "<input type='hidden' id='show_email' name='show_email' value='" + showEmail + "'>";
		showEmailChk += "<span class='small'>プロフィールで公開する</span>";
		showEmailChk += "</div>";

		if( column.equals("email") ){
			if(this.memberData.get("openUserId")!=null && !this.memberData.get("openUserId").isEmpty()){
				if(!value.isEmpty()){
					str += value;
					str += showEmailChk;
				}
			}else{
				str += "<input "
						+ "type='text' "
						+ "id='" + column + "' "
						+ "name='" + column + "' "
						+ "value='" + value + "' "
						+ "style='ime-mode:disabled;'"
						+ "> ";
				str += "<span class='notice small'>※必須</span>";
				str += "<div><label class='invalid' for='"+column+"' generated='true' style='display:none;'></label></div>";
				str += showEmailChk;
			}
		}else if(column.equals("nickName")){
			str = "<input "
					+ "type='text' "
					+ "id='" + column + "' "
					+ "name='" + column + "' "
					+ "value='" + value + "' "
					+ "> ";
			str += "<div><label class='invalid' for='"+column+"' generated='true' style='display:none;'></label></div>";
		}else if(column.equals("password")){
			str = "<input "
					+ "type='password' "
					+ "id='password1' "
					+ "name='password1' "
					+ "> ";
			str += "<span class='notice small'>※必須</span>";
			str += "<div><label class='invalid' for='password1' generated='true' style='display:none;'></label></div>";
			str += "<div>";
			str += "<input "
					+ "type='password' "
					+ "id='password2' "
					+ "name='password2' "
					+ ">";
			str += "<span class='small bold'>（確認)</span>";
			str += "</div>";
			str += "<div><label class='invalid' for='password2' generated='true' style='display:none;'></label></div>";
		}else if(column.equals("birthday")){
			String valueYear = "";
			String valueMonth = "";
			String valueDay = "";
			if(value.length()==8){
				valueYear = value.substring(0,4);
				valueMonth = value.substring(4,6);
				valueDay = value.substring(6,8);
			}
			str = "<input "
					+ "type='text' "
					+ "id='birth_year' "
					+ "name='birth_year' "
					+ "value='" + valueYear + "' "
					+ "style='ime-mode:disabled;' "
					+ "maxlength='4'"
					+ "> 年&nbsp;";
			str += "<select id='birth_month' name='birth_month'>";
			
			for(int i=1; i<13; i++){
				str += "<option value='" + String.format("%1$02d",i) + "'";
				if(valueMonth.equals(String.format("%1$02d",i))){
					str += " selected";
				}
				str += ">";
				str += String.valueOf(i);
				str += "</option>";
			}
			str += "</select>月&nbsp;";
			
			str += "<select id='birth_day' name='birth_day'>";
			for(int i=1; i<32; i++){
				str += "<option value='" + String.format("%1$02d",i) + "'";
				if(valueDay.equals(String.format("%1$02d",i))){
					str += " selected";
				}
				str += ">";
				str += String.valueOf(i);
				str += "</option>";
			}
			str += "</select>日";
			str += "<div><label class='invalid' for='birth_year' generated='true' style='display:none;'></label></div>";
		}else if(column.equals("sex")){
			str = "<input type='radio' class='sex' name='sex' value='"+classMember.MALE + "'";
			if(classMember.MALE.equals(value)){
				str += " checked";
			}
			str += ">";
			str += classMember.MALE;
			str += "<input type='radio' class='sex' name='sex' value='"+classMember.FEMALE + "'";
			if(classMember.FEMALE.equals(value)){
				str += " checked";
			}
			str += ">";
			str += classMember.FEMALE;
		}else if(column.equals("home")){
			str = "<div id='dummy_home'></div>";
			str += "<select name='home' id='home' onClick='popUpPrefecture();'>";
			str += "<option value=''>都道府県をお選びください。</option>";
			str += "<optgroup label='北海道・東北'>";
			str += "<option value='北海道'"; if(value.equals("北海道")){ str += " selected"; } str += ">北海道</option>";
			str += "<option value='青森県'"; if(value.equals("青森県")){ str += " selected"; } str += ">青森県</option>";
			str += "<option value='秋田県'"; if(value.equals("秋田県")){ str += " selected"; } str += ">秋田県</option>";
			str += "<option value='岩手県'"; if(value.equals("岩手県")){ str += " selected"; } str += ">岩手県</option>";
			str += "<option value='山形県'"; if(value.equals("山形県")){ str += " selected"; } str += ">山形県</option>";
			str += "<option value='宮城県'"; if(value.equals("宮城県")){ str += " selected"; } str += ">宮城県</option>";
			str += "<option value='福島県'"; if(value.equals("福島県")){ str += " selected"; } str += ">福島県</option>";
			str += "</optgroup>";
			str += "<optgroup label='甲信越・北陸'>";
			str += "<option value='山梨県'"; if(value.equals("山梨県")){ str += " selected"; } str += ">山梨県</option>";
			str += "<option value='長野県'"; if(value.equals("長野県")){ str += " selected"; } str += ">長野県</option>";
			str += "<option value='新潟県'"; if(value.equals("新潟県")){ str += " selected"; } str += ">新潟県</option>";
			str += "<option value='富山県'"; if(value.equals("富山県")){ str += " selected"; } str += ">富山県</option>";
			str += "<option value='石川県'"; if(value.equals("石川県")){ str += " selected"; } str += ">石川県</option>";
			str += "<option value='福井県'"; if(value.equals("福井県")){ str += " selected"; } str += ">福井県</option>";
			str += "</optgroup>";
			str += "<optgroup label='関東'>";
			str += "<option value='茨城県'"; if(value.equals("茨城県")){ str += " selected"; } str += ">茨城県</option>";
			str += "<option value='栃木県'"; if(value.equals("栃木県")){ str += " selected"; } str += ">栃木県</option>";
			str += "<option value='群馬県'"; if(value.equals("群馬県")){ str += " selected"; } str += ">群馬県</option>";
			str += "<option value='埼玉県'"; if(value.equals("埼玉県")){ str += " selected"; } str += ">埼玉県</option>";
			str += "<option value='千葉県'"; if(value.equals("千葉県")){ str += " selected"; } str += ">千葉県</option>";
			str += "<option value='東京都'"; if(value.equals("東京都")){ str += " selected"; } str += ">東京都</option>";
			str += "<option value='神奈川県'"; if(value.equals("神奈川県")){ str += " selected"; } str += ">神奈川県</option>";
			str += "</optgroup>";
			str += "<optgroup label='東海'>";
			str += "<option value='愛知県'"; if(value.equals("愛知県")){ str += " selected"; } str += ">愛知県</option>";
			str += "<option value='静岡県'"; if(value.equals("静岡県")){ str += " selected"; } str += ">静岡県</option>";
			str += "<option value='岐阜県'"; if(value.equals("岐阜県")){ str += " selected"; } str += ">岐阜県</option>";
			str += "<option value='三重県'"; if(value.equals("三重県")){ str += " selected"; } str += ">三重県</option>";
			str += "</optgroup>";
			str += "<optgroup label='関西'";
			str += "<option value='大阪府'"; if(value.equals("大阪府")){ str += " selected"; } str += ">大阪府</option>";
			str += "<option value='兵庫県'"; if(value.equals("兵庫県")){ str += " selected"; } str += ">兵庫県</option>";
			str += "<option value='京都府'"; if(value.equals("京都府")){ str += " selected"; } str += ">京都府</option>";
			str += "<option value='滋賀県'"; if(value.equals("滋賀県")){ str += " selected"; } str += ">滋賀県</option>";
			str += "<option value='奈良県'"; if(value.equals("奈良県")){ str += " selected"; } str += ">奈良県</option>";
			str += "<option value='和歌山県'"; if(value.equals("和歌山県")){ str += " selected"; } str += ">和歌山県</option>";
			str += "</optgroup>";
			str += "<optgroup label='中国'>";
			str += "<option value='岡山県'"; if(value.equals("岡山県")){ str += " selected"; } str += ">岡山県</option>";
			str += "<option value='広島県'"; if(value.equals("広島県")){ str += " selected"; } str += ">広島県</option>";
			str += "<option value='鳥取県'"; if(value.equals("鳥取県")){ str += " selected"; } str += ">鳥取県</option>";
			str += "<option value='島根県'"; if(value.equals("島根県")){ str += " selected"; } str += ">島根県</option>";
			str += "<option value='山口県'"; if(value.equals("山口県")){ str += " selected"; } str += ">山口県</option>";
			str += "</optgroup>";
			str += "<optgroup label='四国'>";
			str += "<option value='徳島県'"; if(value.equals("徳島県")){ str += " selected"; } str += ">徳島県</option>";
			str += "<option value='香川県'"; if(value.equals("香川県")){ str += " selected"; } str += ">香川県</option>";
			str += "<option value='愛媛県'"; if(value.equals("愛媛県")){ str += " selected"; } str += ">愛媛県</option>";
			str += "<option value='高知県'"; if(value.equals("高知県")){ str += " selected"; } str += ">高知県</option>";
			str += "</optgroup>";
			str += "<optgroup label='九州・沖縄'>";
			str += "<option value='福岡県'"; if(value.equals("福島県")){ str += " selected"; } str += ">福岡県</option>";
			str += "<option value='佐賀県'"; if(value.equals("佐賀県")){ str += " selected"; } str += ">佐賀県</option>";
			str += "<option value='長崎県'"; if(value.equals("長崎県")){ str += " selected"; } str += ">長崎県</option>";
			str += "<option value='熊本県'"; if(value.equals("熊本県")){ str += " selected"; } str += ">熊本県</option>";
			str += "<option value='大分県'"; if(value.equals("大分県")){ str += " selected"; } str += ">大分県</option>";
			str += "<option value='宮崎県'"; if(value.equals("宮崎県")){ str += " selected"; } str += ">宮崎県</option>";
			str += "<option value='鹿児島県'"; if(value.equals("鹿児島県")){ str += " selected"; } str += ">鹿児島県</option>";
			str += "<option value='沖縄県'"; if(value.equals("沖縄県")){ str += " selected"; } str += ">沖縄県</option>";
			str += "</optgroup>";
			str += "<optgroup label='その他'>";
			str += "<option value='その他'"; if(value.equals("その他")){ str += " selected"; } str += ">その他</option>";
			str += "</optgroup>";
			str += "</select>";
		}else if( column.equals("website") ){
			str = "<input "
					+ "type='text' "
					+ "id='" + column + "' "
					+ "name='" + column + "' "
					+ "value='" + value + "' "
					+ "style='ime-mode:disabled;'"
					+ "> ";
			str += "<div><label class='invalid' for='"+column+"' generated='true' style='display:none;'></label></div>";
		}
		return str;
	}
}
