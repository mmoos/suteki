package com.mowcomi.suteki.global;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.LinkedHashMap;
import javax.servlet.http.HttpSession;

public class PageMenu {
	private GlobalStrings gs = new GlobalStrings();
	private boolean loggedIn;	
	private LinkedHashMap<String,String> menuTitle = new LinkedHashMap<String, String>();
	private String pageTitle = "";
	private String controller = "";
	private LinkedHashMap<String,String> params = new LinkedHashMap<String, String>();
	private String controllerToReturn = "";
	private String paramsToReturn = "";
	private String defaultController = "new";
	
	public PageMenu(){}
	
	private LinkedHashMap<String,String> getMenuTitle(){ return this.menuTitle; }
	public String getPageTitle(){ return this.pageTitle; }
	public String getController(){ return this.controller; }
	public LinkedHashMap<String,String> getParams(){ return this.params; }	
	public String getControllerToReturn(){ return this.controllerToReturn; }
	public String getParamsToReturn(){ return this.paramsToReturn; }
	
	public void setLoginFlag(boolean loggedIn){ 
		this.loggedIn = loggedIn;
		this.setPageMenu();
	}
	
	public void setControllerAndParams(String controller, String params){
		LinkedList<String> args = new LinkedList<String>();
		if( params!=null && !params.isEmpty() ){
			String splitted[] = params.split("/");
			int l = splitted.length;
			String extraMessage = this.getExtraMessage(splitted[l-1]);
			if( !extraMessage.isEmpty() ){
				this.params.put("extra", extraMessage);
				l --;
			}
			for(int i=0; i<l; i++){
				args.add(i,splitted[i]);
			}
		}
		
		if( controller.equals("post") ){
			if(this.notEmpty(args, 0)){
				this.controller = controller;
				this.params.put("postID", args.get(0));
				this.controllerToReturn = this.defaultController;
				this.paramsToReturn = "";
			}
		}else if( 
				controller.equals("new") 
				|| controller.equals("popular") 
				|| ( controller.equals("post_own") && this.loggedIn ) 
				|| ( controller.equals("favorite_own") && this.loggedIn )
						){
			this.controller = controller;
			this.controllerToReturn = controller;
			if(this.notEmpty(args, 0)){
				this.params.put("page", args.get(0));
				this.paramsToReturn = "/"+args.get(0);
			}else{
				this.params.put("page", "1");
				this.paramsToReturn = "";
			}
		}else if( controller.equals("post_other") || controller.equals("favorite_other") ){
			if( !this.notEmpty(args, 0) ){
				this.controller = this.defaultController;
				this.controllerToReturn = this.defaultController;
				this.paramsToReturn = "";
			}else{
				this.controller = controller;
				this.params.put("targetMemberID", args.get(0));
				this.controllerToReturn = controller;
				this.paramsToReturn = "/" + args.get(0);
				if(this.notEmpty(args, 1)){
					this.params.put("page", args.get(1));
					this.paramsToReturn += "/" + args.get(1);
				}
			}
		}else if( controller.equals("profile_own") && this.loggedIn ){
			this.controller = controller;
			this.controllerToReturn = controller;
			this.paramsToReturn = "";
		}else if( controller.equals("profile_other") && this.notEmpty(args, 0) ){
			this.controller = controller;
			this.params.put("targetMemberID", args.get(0));
			this.controllerToReturn = controller;
			this.paramsToReturn = "/" + args.get(0);
		}else{
			this.controller = this.defaultController;
			this.controllerToReturn = this.defaultController;
			this.paramsToReturn = "";
		}
		this.setPageTitle();
	}
	
	private String getExtraMessage(String arg){
		if(arg==null || arg.isEmpty()){ return ""; }
		
		String extraMessage = "";
		if(arg.equals("new_post")){
			extraMessage = "<div class='success'>"+this.gs.ACT_POST+"しました</div>";
		}else if(arg.equals("edit_post")){
			extraMessage = "<div class='success'>"+this.gs.ACT_EDIT+"しました</div>";
		}else if(arg.equals("add_favorite")){
			extraMessage = "<div class='success'>"+this.gs.ACT_FAVORITE+"しました</div>";
		}else if(arg.equals("already_favorite")){
			extraMessage = "<div class='error'>既に"+this.gs.ACT_FAVORITE+"しています</div>";
		}else if(arg.equals("edit_profile")){
			extraMessage = "<div class='success'>"+this.gs.ACT_EDIT+"しました</div>";
		}else if(arg.equals("registered")){
			extraMessage = "<div class='success'>アカウントを登録しました</div>";
		}
		return extraMessage;
	}
	
	private void setPageMenu(){
		this.menuTitle.put("new", "新着" + gs.NAME_POST);
		this.menuTitle.put("popular", "人気" + gs.NAME_POST );
		if( this.loggedIn ){
			this.menuTitle.put("post_own","MY" + gs.NAME_POST);
			this.menuTitle.put("favorite_own",gs.ACT_FAVORITE + gs.NAME_POST);
			this.menuTitle.put("profile_own",gs.NAME_PROFILE);
		}
	}
	
	private void setPageTitle(){
		String nickName = "";
		if(this.params.containsKey("targetMemberID")){
			nickName = gs.getNickName(this.params.get("targetMemberID"));
		}
		
		if(this.controller.equals("post")){ 
			String act = this.params.containsKey("postID") ? gs.ACT_EDIT : gs.ACT_POST ;
			this.pageTitle = gs.NAME_POST + "を" + act +"する";
		}
		else if(this.controller.equals("new")){ this.pageTitle = "新着の" + gs.NAME_POST;}
		else if(this.controller.equals("popular")){ this.pageTitle = "多くのメンバーから"+gs.ACT_FAVORITE+"されている"+gs.NAME_POST;}
		else if(this.controller.equals("post_own")){ this.pageTitle = "あなたが"+gs.ACT_POST+"した"+gs.NAME_POST;}
		else if(this.controller.equals("post_other")){ this.pageTitle = nickName+"が"+gs.ACT_POST+"した"+gs.NAME_POST;}
		else if(this.controller.equals("favorite_own")){ this.pageTitle = "あなたが"+gs.ACT_FAVORITE+"した"+gs.NAME_POST;}
		else if(this.controller.equals("favorite_other")){ this.pageTitle = nickName+"が"+gs.ACT_FAVORITE+"した"+gs.NAME_POST;}
		else if(this.controller.equals("profile_own")){ this.pageTitle = gs.NAME_PROFILE;}
		else if(this.controller.equals("profile_other")){ this.pageTitle = nickName+"の"+gs.NAME_PROFILE;}
	}
	
	private boolean notEmpty(LinkedList<String> args, int i){
		if( args.size() <= i){ return false; }
		if( args.get(i) == null){ return false; }
		if( args.get(i).isEmpty() ){ return false; }
		return true;
	}
		
	public String getMenu(String currentPage){
		String menu = "";
		LinkedHashMap<String,String> pages = this.getMenuTitle();
		for(Iterator<String> i=pages.keySet().iterator(); i.hasNext();){
			String key = i.next().toString();
			String val = pages.get(key);
			if(currentPage.equalsIgnoreCase(key)){
				menu += "<span class='menu_current'>";
			}else{
				menu += "<span class='menu' onClick='location.href=\"/" + key + "\"';>";
			}
			menu += val;
			menu += "</span>";
		}
		return menu;
	}
	
	public String getLinkPost(){ return "/post"; }
	public String getLinkEdit(String postID){ return "/post/"+postID; }
	public String getLinkNew(){ return "/new"; }
	public String getLinkPopular(){ return "/popular"; }
	public String getLinkPostOwn(){ return "/post_own"; }
	public String getLinkPostOther(String memberID){ return "/post_other/" + memberID; }
	public String getLinkFavoriteOwn(){ return "/favorite_own"; }
	public String getLinkFavoriteOther(String memberID){ return "/favorite_other/" + memberID; }
	public String getLinkProfileOwn(){ return "/profile_own";}
	public String getLinkProfileOther(String memberID){ return "/profile_other/" + memberID; }
	
	public String getBackPage(String def, HttpSession session){
		String backPage;
		if( session.getAttribute("controller")!=null && !session.getAttribute("controller").toString().isEmpty() ){
			backPage = "/" + session.getAttribute("controller");
			if( session.getAttribute("params")!=null && !session.getAttribute("params").toString().isEmpty() ){
				backPage += "/" + session.getAttribute("params");
			}
		}else{
			backPage = "/"+def;
		}
		return backPage;
	}
}
