package com.mowcomi.suteki.global;

import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.data.Member;

public class GlobalStrings {
	public String NAME_SERVICE = "ステキ";
	public String NAME_POST = "ステキ";
	public String NAME_PROFILE = "プロフィール";

	public String ACT_POST = "投稿";
	public String ACT_POST_SP = "ステキ";
	public String ACT_EDIT = "編集";
	public String ACT_FAVORITE = "共感";
	public String ACT_COMMENT = "コメント";
	
	public String PROFILE_NICKNAME = "ニックネーム";
	public String PROFILE_EMAIL = "メールアドレス";
	public String PROFILE_PASSWORD = "パスワード";
	public String PROFILE_BIRTHDAY = "生年月日";
	public String PROFILE_AGE = "年齢";
	public String PROFILE_SEX = "性別";
	public String PROFILE_HOME = "住んでいる地域";
	public String PROFILE_WEBSITE = "サイトURL";

	public GlobalStrings(){}
	
	public String getNickName(String memberID){
		String nickName;
		if( memberID==null || memberID.isEmpty() ){
			nickName = "ゲストさん";
		}else{
			Member classMember = new Member();
			Row theMember = classMember.getByID(memberID);
			if( theMember!=null && theMember.get("nickName")!=null && !theMember.get("nickName").isEmpty() ){
				nickName = theMember.get("nickName");
			}else{
				nickName = "？？？";
			}
		}
		return nickName;
	}
		
	public String getPsudoButton(String onClick, String val){
		return "<span class='psudo_button' onClick='"+onClick+"'>"+val+"</span>";
	}
		
	private String getPagingHref(String baseURI, int page){
		return "<a href='" + baseURI + "/" + String.valueOf(page) + "'>";
	}
	
	private String getPagingSpan(String baseURI, int page, String text){
		return "<span class='page_text'>" + this.getPagingHref(baseURI, page) 
				+ text + "</a></span>";		
	}
	
	private String getPagingSpan(String baseURI, int page){
		return "<span class='page_num'>" + this.getPagingHref(baseURI, page) 
				+ String.valueOf(page) + "</a></span>";
	}
	
	public String getPaging(String baseURI, int maxPage, String page, int neighbor){
		int intPage = (page==null || page.isEmpty()) ? 1 : Integer.parseInt(page) ;
		return this.getPaging(baseURI, maxPage, intPage, neighbor);
	}
	
	public String getPaging(String baseURI, int maxPage, int page, int neighbor){
		if(maxPage<2){ return ""; }
		
		String prev = "";
		String first = "";
		String firstInterval = "";
		String prevNeighbor = "";
		String self = "";
		String nextNeighbor = "";
		String lastInterval = "";
		String last = "";
		String next = "";
		if(page>1){
			 prev = this.getPagingSpan(baseURI, page-1, "前へ");
			 first = this.getPagingSpan(baseURI, 1);
		}
		if( page - neighbor > 2 ){
			firstInterval = "...";
		}
		for(int i=page-neighbor; i<page; i++){
			if(i>1){
				prevNeighbor += this.getPagingSpan(baseURI, i);
			}
		}
		self = "<span class='page_self'>" + String.valueOf(page) + "</span>";
		for(int i=page+1; i<page+neighbor; i++){
			if(i<maxPage){
				nextNeighbor += this.getPagingSpan(baseURI, i);
			}
		}
		if( page + neighbor < maxPage ){
			lastInterval = "...";
		}
		if(page<maxPage){
			last = this.getPagingSpan(baseURI, maxPage);
			next = this.getPagingSpan(baseURI, page+1, "次へ");
		}
		
		return "<div class='paging'>" + prev + first + firstInterval + prevNeighbor + self 
				+ nextNeighbor + lastInterval + last + next + "</div>";
	}
	
	public String[][] getOpenIdProviders(){
		String[][] openIdProviders = {
				{"google", "https://www.google.com/accounts/o8/id"}
				, {"yahoo", "http://www.yahoo.co.jp/"}
				, {"twitter", "twitter"}
				, {"mixi", "https://mixi.jp/"}
				};
        return openIdProviders;
	}
}
