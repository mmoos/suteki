package com.mowcomi.suteki.global;

import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {
	public String fromAddress = "admin@sutekitest.appspotmail.com";
	public String fromName = "suteki";
	
	public boolean send(String toAddress, String toName, String subject, String msgBody){
	    Properties props = new Properties();
	    Session session = Session.getDefaultInstance(props, null);
	    try {
	        MimeMessage msg = new MimeMessage(session);
	        try{
	        	msg.setFrom(new InternetAddress(this.fromAddress,this.fromName,"iso-2022-jp"));
	        }catch(Exception e){
	        	return false;
	        }
	        try{
	        	msg.addRecipient(MimeMessage.RecipientType.TO,new InternetAddress(toAddress, toName,"iso-2022-jp"));
	        }catch(Exception e){
	        	return false;
	        }
	        msg.setSubject(subject,"iso-2022-jp");
	        msg.setText(msgBody);
	        Transport.send(msg);
	        return true;
	    } catch (AddressException e) {
	        return false;
	    } catch (MessagingException e) {
	        return false;
	    }	
	}
}
