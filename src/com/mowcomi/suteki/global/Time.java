package com.mowcomi.suteki.global;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Time {
	public String US_HAWAII = "US/Hawaii";
	public String US_WEST = "America/Los_Angeles";
	public String US_MIDDLE = "America/Belize";
	public String US_EAST = "America/New_York";
	public String GMT = "Europe/London";
	public String EUROPE_MIDDLE = "Europe/Paris";
	public String EUROPE_EAST = "Africa/Cairo";
	public String MIDDLE_EAST = "Asia/Kuwait";
	public String RUSSIA = "Europe/Moscow";
	public String INDIA = "Asia/Calcutta";
	public String JAPAN = "Asia/Tokyo";
	
	public Time(){}
		
	public String getTimeString(String format, int addSec, String zone){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, addSec);
		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getTimeZone(zone));
		return df.format(cal.getTime());	
	}
	
	public String getTimeString(String format, int addSec){
		return this.getTimeString(format, addSec, this.JAPAN);
	}
	
	public String convertTimeZone(String original, String format, String toZone, String fromZone){
		DateFormat fromFormat = new SimpleDateFormat(format);
		fromFormat.setTimeZone(TimeZone.getTimeZone(fromZone));
		DateFormat toFormat = new SimpleDateFormat(format);
		toFormat.setTimeZone(TimeZone.getTimeZone(toZone));
		try{
			Date d = fromFormat.parse(original);
			return toFormat.format(d);
		}catch(Exception e){
			return null;
		}
	}
	
	public String convertTimeZone(String original, String format, String toZone){
		return this.convertTimeZone(original, format, toZone, this.JAPAN);
	}
}
