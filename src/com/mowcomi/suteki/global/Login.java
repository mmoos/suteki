package com.mowcomi.suteki.global;

import com.mowcomi.suteki.data.Member;
import com.mowcomi.suteki.row.Row;
import java.util.LinkedHashMap;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

public class Login {
	private int COOKIE_AGE = 60*60*24*30;
	private String COOKIE_PASH = "/";
	private boolean saveCookieFlag = true;

	private HttpServletRequest req;
	private HttpServletResponse resp;

	public String memberID = "";
	public String openUserId = "";
	public String email = "";
	public String password = "";
	public String nickName = "";

	public void dontSaveCookie(){this.saveCookieFlag = false;}
	
	public String getMemberID() {return memberID;}
	public void setMemberID(String memberID) {this.memberID = memberID;}
	public String getOpenUserId() {return openUserId;}
	public void setOpenUserId(String openUserId) {this.openUserId = openUserId;}
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}	
	public String getPassword() {return password;}
	public void setPassword(String password) {this.password = password;}
	public String getNickName() {return nickName;}
	public void setNickName(String nickName) {this.nickName = nickName;}

	public Login(HttpServletRequest req, HttpServletResponse resp){
		this.req = req; 
		this.resp= resp;
	}
		
	public String getBackPage(){
		PageMenu pageMenu = new PageMenu();
		return pageMenu.getBackPage("", this.req.getSession());
	}
	
	public String getLoginPage(){
		return "/login/open";
	}
	
	public String getLoginErrorPage(){
		return "/login/open/error";
	}
	
	public String getLogoutPage(){
		return "/servlet/logout";
	}
	
	public void saveSession(){
		this.req.getSession().setAttribute("email", this.email);
		this.req.getSession().setAttribute("nickName", this.nickName);
		this.req.getSession().setAttribute("memberID", this.memberID);
	}
	
	public void saveCookie(){
		Cookie cookieEmail = new Cookie("email", this.email);
	    cookieEmail.setMaxAge(this.COOKIE_AGE);
	    cookieEmail.setPath(this.COOKIE_PASH);
		this.resp.addCookie(cookieEmail);
		Cookie cookiePassword = new Cookie("password", this.password);
	    cookiePassword.setMaxAge(this.COOKIE_AGE);
	    cookiePassword.setPath(this.COOKIE_PASH);
		this.resp.addCookie(cookiePassword);
	}
	
	public void register(){
		Member classMember = new Member();
		LinkedHashMap<String,String> memberData = new LinkedHashMap<String,String>();
		memberData.put("email",this.email);
		memberData.put("nickName",this.nickName);
		memberData.put("openUserId",this.openUserId);
		memberData.put("password",this.password);
		memberData.put("show_email", classMember.NEVER_SHOW);
		String newID = classMember.newMember(memberData);
		this.setMemberID(newID);
		this.saveSession();
		if(this.saveCookieFlag){
			this.saveCookie();
		}
	}
	
	public void setResult(Row memberData){
		this.setEmail(memberData.get("email"));
		this.setNickName(memberData.get("nickName"));
		this.setMemberID(memberData.getSeqID());
		this.saveSession();
		if(this.saveCookieFlag){
			this.saveCookie();
		}
	}
	
	public boolean tryLogin(){
		if( this.setMemberInfoWithSession() ){
			return true;
		}
		if( this.setMemberInfoWithCookie() ){
			return true;
		}
		if( this.setMemberInfoWithUserService() ){
			return true;
		}
		return false;
	}
	
	public void loginWithOpenID(){
		this.dontSaveCookie();
		Member classMember = new Member();
		Row memberData = classMember.getMemberByOpenID(this.openUserId);
		if(memberData==null){
			this.register();
		}else{
			this.setResult(memberData);
		}
	}
	
	public boolean loginWithEmail(){
		if( this.email==null || this.email.isEmpty() ){
			return false;
		}
		if( this.password==null || this.password.isEmpty() ){
			return false;
		}
		Member classMember = new Member();
		Row memberData = classMember.getMemberByEmail(this.email, this.password);
		if(memberData==null){
			return false;
		}else{
			this.setResult(memberData);
			return true;
		}
	}
	
	public boolean setMemberInfoWithSession(){
		if(this.req.getSession() == null
				|| this.req.getSession().getAttribute("memberID")==null 
				|| this.req.getSession().getAttribute("email")==null
				|| this.req.getSession().getAttribute("nickName")==null
		){
			return false;
		}else{
			this.setMemberID(this.req.getSession().getAttribute("memberID").toString());
			this.setEmail(this.req.getSession().getAttribute("email").toString());
			this.setNickName(this.req.getSession().getAttribute("nickName").toString());
			return true;
		}
	}
	
	public boolean setMemberInfoWithCookie(){
		if(this.req.getCookies()==null || this.req.getCookies().length==0){
			return false;
		}
		
		boolean foundEmail = false;
		boolean foundPass = false;
		
		Cookie cookies[] = this.req.getCookies();
		for(int i=0; i<cookies.length; i++){
			if(cookies[i].getName().equals("email")){
				if(cookies[i].getValue()==null || cookies[i].getValue().isEmpty()){
					return false;
				}
				this.email = cookies[i].getValue();
				foundEmail = true;
			}
			if(cookies[i].getName().equals("password")){
				if(cookies[i].getValue()==null || cookies[i].getValue().isEmpty()){
					return false;
				}
				this.password = cookies[i].getValue();
				foundPass = true;
			}
			if(foundEmail && foundPass){
				this.saveCookie();
				break;
			}
		}
		return this.loginWithEmail();
	}
	
	public boolean setMemberInfoWithUserService(){
		UserService userService = UserServiceFactory.getUserService();
	    User user = userService.getCurrentUser();
	    if(user == null){
	    	return false;
	    }else{
	    	this.setOpenUserId(user.getUserId());
	    	this.setEmail(user.getEmail());
	    	this.setNickName(user.getNickname());
	    	this.loginWithOpenID();
	    	return true;
	    }
	}
	
	public void clearSession(){
		this.req.getSession().removeAttribute("email");
		this.req.getSession().removeAttribute("nickName");
		this.req.getSession().removeAttribute("memberID");		
	}
	
	public void clearCookie(){
		Cookie cookieEmail = new Cookie("email", "");
	    cookieEmail.setMaxAge(0);
	    cookieEmail.setPath(this.COOKIE_PASH);
		this.resp.addCookie(cookieEmail);
		Cookie cookiePassword = new Cookie("password", "");
	    cookiePassword.setMaxAge(0);
	    cookiePassword.setPath(this.COOKIE_PASH);
		this.resp.addCookie(cookiePassword);
	}
	
	public void logout(){
		this.clearSession();
		this.clearCookie();
	}
}
