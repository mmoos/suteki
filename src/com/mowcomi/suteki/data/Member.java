package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.row.MemberRow;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Member extends DataSuper{
	public String NEVER_SHOW = "0";
	public String ALWAYS_SHOW = "1";
	public String MALE = "男性";
	public String FEMALE = "女性";
	
	public Member(){ super(); }
	
	@Override
	public Row getNewRow(){
		return new MemberRow(null,null,null,null,
				null,null,null,null,null,null,
				null,null,null,null,null,null);
	}
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(MemberRow.class); }
	@Override
	public String getSeqName(){ return "com.mowcomi.suteki.row.MemberSequence"; }
	@Override
	public Row getByID(String seqID){ 
		try{
			return this.pm.getObjectById(MemberRow.class, seqID);
		}catch(Exception e){
			return null;
		}
	}
	
	public String encryptPassword(String password){
        String key = "sutekisuteki";
        String algorithm = "BLOWFISH";
        SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), algorithm);
        try{
        	Cipher cipher = Cipher.getInstance(algorithm);
        	cipher.init(Cipher.ENCRYPT_MODE, sksSpec);
        	byte[] encrypted = cipher.doFinal(password.getBytes());
        	return new String(encrypted, "UTF-8"); 	
        }catch(Exception e){
        	return e.getMessage();
        }
	}
	
	public Row getMemberByID(String memberID){
		if(memberID==null || memberID.isEmpty()){
			return null;
		}
		return this.getByID(memberID);
	}
	
	public Row getMemberByOpenID(String openUserId){
		if(openUserId==null || openUserId.isEmpty()){
			return null;
		}
		this.setNewQuery();
		this.setWhere("openUserId", this.EQUAL, openUserId);
		return this.executeSelectOne();
	}
	
	public Row getMemberByEmail(String email, String password){
		if(email==null || email.isEmpty() || password==null || password.isEmpty()){
			return null;
		}
		this.setNewQuery();
		this.setWhere("email", this.EQUAL, email);
		this.setWhere("password", this.EQUAL, this.encryptPassword(password));
		return this.executeSelectOne();
	}
	
	private Row putMemberData(Row row, LinkedHashMap<String,String> memberData){
		if(memberData!=null && !memberData.isEmpty()){
			for(Iterator<String> i=memberData.keySet().iterator(); i.hasNext();){
				String column = i.next();
				if(column.equals("password")){
					row.set(column, this.encryptPassword(memberData.get(column)));
				}else{
					row.set(column, memberData.get(column));
				}
			}
		}
		return row;
	}
	
	public String newMember(LinkedHashMap<String,String> memberData){
		Row newRow = this.getNewRow();
		newRow = this.putMemberData(newRow, memberData);
		return this.executeInsert(newRow);
	}
	
	public void editProfile(String memberID, LinkedHashMap<String,String> memberData){
		Row memberRow = this.getMemberByID(memberID);
		memberRow = this.putMemberData(memberRow, memberData);
		this.executeUpdate(memberRow);
	}
	
	public Row getMemberByEmailOnly(String email){
		this.setNewQuery();
		this.setWhere("email", this.EQUAL, email);
		List<Row> result = this.executeSelect();
		if(result==null || result.isEmpty()){
			return null;
		}
		for(int i=0; i<result.size(); i++){
			Row row = result.get(i);
			if(row.get("openUserId")==null || row.get("openUserId").isEmpty()){
				return row;
			}
		}
		return null;
	}
	
	public boolean isAvailableEmail(String email){
		return this.getMemberByEmailOnly(email)==null;
	}

	public boolean isAvailableNickName(String value){
		this.setNewQuery();
		this.setWhere("nickName", this.EQUAL, value);
		List<Row> result = this.executeSelect();
		if(result==null || result.isEmpty()){
			return true;
		}
		return false;		
	}
}
