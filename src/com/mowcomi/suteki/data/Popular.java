package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.row.PopularRow;
import java.util.List;
import java.util.LinkedList;

public class Popular extends DataSuper{	
	private final int maxByAnonymous = 20;
	private final int byAnonymousPt = 1;
	private final int byMemberPt = 5;
	
	public Popular(){ super(); }
	
	@Override
	public Row getNewRow(){	return new PopularRow(null,null,null,null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(PopularRow.class); }
	@Override
	public Row getByID(String seqID){ 
		try{
			return this.pm.getObjectById(PopularRow.class, seqID);
		}catch(Exception e){
			return null;
		}
	}
	
	public LinkedList<String> getPopularList(){
		this.setNewQuery();
		List<Row> resultList = this.executeSelect("whole desc");
		if(resultList==null || resultList.isEmpty()){
			return null;
		}
		LinkedList<String> popularList = new LinkedList<String>();
		for(int i=0; i<resultList.size(); i++){
			popularList.addLast(resultList.get(i).get("postID"));
		}
		return popularList;
	}
	
	private String makePt(int p){
		return String.format("%1$05d", p);
	}
	
	public void addCnt(String postID, boolean byMember){
		this.setNewQuery();
		this.setWhere("postID", this.EQUAL, postID);
		Row already = this.executeSelectOne();
		if(already==null){
			Row newRow = this.getNewRow();
			newRow.set("postID", postID);
			if(byMember){
				newRow.set("whole", this.makePt(this.byMemberPt));
				newRow.set("byMember", this.makePt(this.byMemberPt));
				newRow.set("byAnonymous", this.makePt(0));
			}else{
				newRow.set("whole", this.makePt(this.byAnonymousPt));
				newRow.set("byMember", this.makePt(0));
				newRow.set("byAnonymous", this.makePt(this.byAnonymousPt));
			}
			this.executeInsert(newRow,postID);
		}else if(!byMember){
			int oldAnonymous = Integer.parseInt(already.get("byAnonymous"));
			if(oldAnonymous < this.maxByAnonymous ){
				int oldWhole = Integer.parseInt(already.get("whole"));
				already.set("whole", this.makePt(oldWhole+this.byAnonymousPt));
				already.set("byAnonymous", this.makePt(oldAnonymous+this.byAnonymousPt));
				this.executeUpdate(already);
			}
		}else{
			int oldWhole = Integer.parseInt(already.get("whole"));
			int oldMember = Integer.parseInt(already.get("byMember"));
			already.set("whole", this.makePt(oldWhole+this.byMemberPt));
			already.set("byMember", this.makePt(oldMember+this.byMemberPt));
			this.executeUpdate(already);
		}
	}	
}
