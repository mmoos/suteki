package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.ImageRow;

public class Image extends DataSuper{
	public Image(){ super(); }
	
	@Override
	public ImageRow getNewRow(){return new ImageRow(null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(ImageRow.class); }
	@Override
	public ImageRow getByID(String seqID){ 
		try{
			return this.pm.getObjectById(ImageRow.class, seqID);
		}catch(Exception e){
			return null;
		}
	}
	@Override
	public String getSeqName(){ return "com.mowcomi.suteki.row.ImageSequence"; }
	
	public String newImage(byte[] b){
		ImageRow newRow = this.getNewRow();
		newRow.setImg(b);
		return this.executeInsert(newRow);
	}
}
