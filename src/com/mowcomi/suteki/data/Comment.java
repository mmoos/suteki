package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.CommentRow;
import com.mowcomi.suteki.row.Row;
import java.util.List;
import java.util.LinkedHashMap;

public class Comment extends DataSuper{	
	public Comment(){ super(); }

	@Override
	public Row getNewRow(){	return new CommentRow(null,null,null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(CommentRow.class); }
	@Override
	public Row getByID(String seqID){ 
		try{
			return this.pm.getObjectById(CommentRow.class, seqID);
		}catch(Exception e){
			return null;
		}
	}
	@Override
	public String getSeqName(){ return "com.mowcomi.suteki.row.CommentSequence"; }
	
	public LinkedHashMap<String,Row> getCommentBySeq(List<String> listSeqID){
		if(listSeqID==null || listSeqID.isEmpty()){
			return null;
		}
		LinkedHashMap<String,Row> results = new LinkedHashMap<String,Row>();
		for(int i=0; i<listSeqID.size(); i++){
			String seqID = listSeqID.get(i);
			results.put(seqID, this.getByID(seqID));
		}
		return results;
	}
	
	public LinkedHashMap<String,Row> searchCommentByMember(String memberID){
		if(memberID==null || memberID.isEmpty()){
			return null;
		}
		
		this.setNewQuery();
		this.setWhere("memberID", this.EQUAL, memberID);
		List<Row> result = this.executeSelect();
		return this.makeMap("seqID", result);
	}
	
	public LinkedHashMap<String,Row> searchCommentByPost(String postID){
		if(postID==null || postID.isEmpty()){
			return null;
		}
		
		this.setNewQuery();
		this.setWhere("postID", this.EQUAL, postID);
		List<Row> result = this.executeSelect();
		return this.makeMap("seqID", result);
	}
		
	public boolean newComment(String memberID, String postID, String content){
		Row newRow = this.getNewRow();
		newRow.set("memberID", memberID);
		newRow.set("postID", postID);
		newRow.set("content", content);
		if(this.executeInsert(newRow)==null){
			return false;
		}else{
			return true;
		}
	}
}
