package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.ReminderRow;
import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.global.Time;

public class Reminder extends DataSuper{
	public int expireSecond = 60*60*24;
	public String expireTime = "24時間";
	
	public Reminder(){ super(); }
	
	@Override
	public Row getNewRow(){ return new ReminderRow(null,null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(ReminderRow.class); }
	@Override
	public Row getByID(String seqID){
		try{
			return this.pm.getObjectById(ReminderRow.class, seqID);
		}catch(Exception e){
			return null;
		}
	}
		
	private String makeHash(String input){
		String hash = "";
		for(int i=0; i<input.length(); i=i+4){
			String sub = ( i+4 > input.length() ) ? input.substring(i) : input.substring(i, i+4);
			hash += Integer.toHexString(Integer.parseInt(sub));
		}
		return hash;
	}
	
	public boolean checkExpire(String hashID){
		Row row = this.getByID(hashID);
		if(row==null || row.getSeqID().isEmpty()){
			return false;
		}
		Time classTime = new Time();
		String now = classTime.getTimeString("yyyyMMddHHmmss", 0);
		if( now.compareTo(row.get("expire")) < 0){
			return true;
		}else{
			this.executeDelete(row);
			return false;
		}
	}
	
	public String getMemberIDbyHash(String hashID){
		String memberID = null;
		Row row = this.getByID(hashID);
		if(row==null || row.getSeqID().isEmpty()){
			return null;
		}
		Time classTime = new Time();
		String now = classTime.getTimeString("yyyyMMddHHmmss", 0);
		if(now.compareTo(row.get("expire")) < 0){
			memberID = row.get("memberID");
		}
		this.executeDelete(row);
		return memberID;
	}
	
	public String newReminder(String memberID){
		if(memberID==null || memberID.isEmpty()){
			return null;
		}
		Time classTime = new Time();
		String now = classTime.getTimeString("yyyyMMddHHmmss", 0);
		String hashID = this.makeHash(memberID.concat(now));		
		String expire = classTime.getTimeString("yyyyMMddHHmmss", this.expireSecond);

		Row newRow = this.getNewRow();
		newRow.set("memberID", memberID);
		newRow.set("expire", expire);
		if(!this.executeInsert(newRow, hashID)){
			return null;
		}
		return hashID;
	}
}