package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.PostRow;
import com.mowcomi.suteki.row.Row;
import java.util.List;
import java.util.LinkedHashMap;

public class Post extends DataSuper{
	public Post(){ super(); }
	
	@Override
	public Row getNewRow(){ return new PostRow(null,null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(PostRow.class); }
	@Override
	public Row getByID(String seqID){
		try{
			return this.pm.getObjectById(PostRow.class, seqID); 
		}catch(Exception e){
			return null;
		}
	}
	@Override
	public String getSeqName(){ return "com.mowcomi.suteki.row.PostSequence"; }
	
	public Row getPostOne(String postID){
		if(postID==null || postID.isEmpty()){
			return null;
		}
		return (Row)this.pm.getObjectById("postID");
	}
	
	public LinkedHashMap<String,Row> getPosts(){
		this.setNewQuery();
		List<Row> result = this.executeSelect();
		return this.makeMap("seqID", result);
	}
	
	public LinkedHashMap<String,Row> getPostsByID(List<String> listSeqID){
		if(listSeqID==null || listSeqID.isEmpty()){
			return null;
		}
		LinkedHashMap<String,Row> posts = new LinkedHashMap<String,Row>();
		for(int i=0; i<listSeqID.size(); i++){
			String seqID = listSeqID.get(i);
			posts.put(seqID, this.getByID(seqID));
		}
		return posts;
	}
	
	public LinkedHashMap<String,Row> getPostsByMember(String memberID){
		this.setNewQuery();
		this.setWhere("memberID", this.EQUAL, memberID);
		List<Row> result = this.executeSelect();
		return this.makeMap("seqID", result);
	}
	
	public String newPost(String memberID, String content){
		Row newRow = this.getNewRow();
		newRow.set("memberID", memberID);
		newRow.set("content", content);
		return this.executeInsert(newRow);
	}
	
	public void editPost(String postID, String content){
		Row toBeUpdated = this.getByID(postID);
		toBeUpdated.set("content", content);
		this.executeUpdate(toBeUpdated);
	}
}
