package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.FavoriteRow;
import com.mowcomi.suteki.row.Row;
import java.util.LinkedList;
import java.util.List;

public class Favorite extends DataSuper{
	public Favorite(){ super(); }

	@Override
	public Row getNewRow(){ return new FavoriteRow(null,null,null,null,null); }
	@Override
	public void setNewQuery(){ this.q = this.pm.newQuery(FavoriteRow.class); }
	@Override
	public Row getByID(String seqID){ 
		try{
			return this.pm.getObjectById(FavoriteRow.class, seqID); 
		}catch(Exception e){
			return null;
		}
	}
	
	public LinkedList<String> getList(String target, String id){
		if(id==null || id.isEmpty()){
			return null;
		}
		
		String filterParam;
		if(target.equals("memberID")){
			filterParam = "postID";
		}else if(target.equals("postID")){
			filterParam = "memberID";
		}else{
			return null;
		}
		
		this.setNewQuery();
		this.setWhere(filterParam, this.EQUAL, id);
		List<Row> result = this.executeSelect();
		
		if(result==null || result.isEmpty()){
			return null;
		}
		
		LinkedList<String> returnList = new LinkedList<String>();
		for(int i=0; i<result.size(); i++){
			returnList.add(result.get(i).get(target));
		}
		return returnList;
	}
	
	public boolean newFavorite(String memberID, String postID){
		Row newRow = this.getNewRow();
		newRow.set("memberID", memberID);
		newRow.set("postID", postID);
		return this.executeInsert(newRow, memberID+"_"+postID);
	}
}