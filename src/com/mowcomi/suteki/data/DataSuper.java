package com.mowcomi.suteki.data;

import com.mowcomi.suteki.row.Row;
import java.util.List;
import java.util.LinkedHashMap;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class DataSuper {
	public PersistenceManager pm;

	public Query q;
	public String where = "";
	public String parameters = "";
	public LinkedHashMap<String,String> args = new LinkedHashMap<String,String>();
	public int offset = 0;
	public int limit = 0;
	public int maxPage;
	public String sort = "created_at desc";

	public final String EQUAL = " == ";
	public final String NOT_EQUAL = " != ";
	public final String GREATER_THAN = " > ";
	public final String GREATER_THAN_OR_EQUAL = " >= ";
	public final String LESS_THAN = " < ";
	public final String LESS_THAN_OR_EQUAL = " <= ";
	
	public DataSuper(){
		this.pm = PMF.get().getPersistenceManager();
	}
	
	public Row getNewRow(){ return new Row(null,null,null); }
	public void setNewQuery(){}
	public Row getByID(String seqID){ return null; }
	public String getSeqName(){ return null; }
	
	public void setOffset(int offset){ this.offset = offset; }
	public void setLimit(int limit){ this.limit = limit; }
	public void setPage(int page){
		if(page==0){ page = 1; }
		this.setOffset( (page-1) * this.limit );
	}
	public void setPage(String strPage){
		int page = (strPage==null || strPage.isEmpty()) ? 1 : Integer.parseInt(strPage);
		this.setPage(page);
	}
	
	public void setWhere(String column, String operator, String value){
		this.setWhere(column, operator, value, "param"+column);
	}
	
	public void setWhere(String column, String operator, String value, String paramName){
		String andWhere = "";
		String andParameter = "";
		if( !this.where.isEmpty() ){
			andWhere = " && ";
			andParameter = " , ";
		}
		this.where += andWhere + " ( " + column + operator + paramName + " ) ";
		this.parameters += andParameter + "String " + paramName;
		this.args.put(paramName, value);
	}
	
	public int getCnt(){
		this.q.setResult("count(this)");
		if(this.args==null || this.args.isEmpty()){
			return (Integer) q.execute();
		}else{
			return (Integer) q.executeWithMap(this.args);
		}
	}
	
	public int getMaxPage(){return this.maxPage;}
	public void setMaxPage(){
		int all = this.getCnt();
		if( this.limit > 0 ){
			this.maxPage = (int) all/this.limit;
			if( all % this.limit > 0 ){
				this.maxPage ++;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Row> executeSelect(String additionalSort){
		if(!this.where.isEmpty()){
			this.q.setFilter(this.where);
			this.q.declareParameters(this.parameters);
		}
		
		if(this.limit>0){
			this.setMaxPage();
		}
		this.q.setResult("this");
		if(additionalSort!=null && !additionalSort.isEmpty()){
			this.sort = additionalSort + " , " + this.sort;
		}
		this.q.setOrdering(this.sort);
		if(this.limit > 0){
			this.q.setRange(this.offset, this.offset + this.limit);
		}
		try{
			if(this.args==null || this.args.isEmpty()){
				return (List<Row>) this.q.execute();
			}else{				
				return (List<Row>) this.q.executeWithMap(this.args);
			}
		}catch(Exception e){
			return null;
		}
	}
	
	public List<Row> executeSelect(){
		return this.executeSelect(null);
	}
	
	public Row executeSelectOne(){
		if(!this.where.isEmpty()){
			this.q.setFilter(this.where);
			this.q.declareParameters(this.parameters);
		}

		try{
			this.q.setUnique(true);
			if(this.args==null || this.args.isEmpty()){
				return (Row)this.q.execute();
			}else{
				return (Row)this.q.executeWithMap(this.args);
			}
		}catch(Exception e){
			return null;
		}
	}

	public LinkedHashMap<String,Row> makeMap(String key, List<Row> resultRow){
		if(resultRow==null || resultRow.isEmpty()){
			return null;
		}
		
		Row row;
		String mapKey;
		LinkedHashMap<String,Row> resultMap = new LinkedHashMap<String,Row>();
		for(int i=0; i<resultRow.size(); i++){
			row = resultRow.get(i);
			if(key.equals("seqID")){
				mapKey = row.getSeqID();
			}else{
				mapKey = row.get(key);
			}
			resultMap.put(mapKey, row);
		}
		return resultMap;
	}
	
	public String executeInsert(Row row){
		Long newID = this.pm.getSequence(this.getSeqName()).nextValue();
		String seqID = Long.toString(newID);
		row.setSeqID(seqID);
		row.setCreatedAt(0);
		row.setUpdatedAt(0);
		try{
			this.pm.makePersistent(row);
			return seqID;
		}catch(Exception e){
			return null;
		}
	}
	
	public boolean executeInsert(Row row, String unique){
		row.setSeqID(unique);
		row.setCreatedAt(0);
		row.setUpdatedAt(0);
		try{
			this.pm.makePersistent(row);
			this.pm.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public void executeUpdate(Row row){
		row.setUpdatedAt(0);
		this.pm.close();
	}
	
	public boolean executeDelete(Row row){
		try{
			this.pm.deletePersistent(row);
			this.pm.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}