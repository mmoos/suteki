package com.mowcomi.suteki.contents;

import com.mowcomi.suteki.global.SendMail;
import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.data.Member;
import com.mowcomi.suteki.data.Reminder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentReminder extends Content {
	public ContentReminder(HttpServletRequest req, HttpServletResponse resp){
		super(req,resp);
		String[] css = {"login"};
		this.setCss(css);
		String[] js = new String[0];
		this.setJs(js);
	}
	
	@Override
	public String getOutPut(){
		String inputEmail = this.req.getParameter("email")==null ? "" : this.req.getParameter("email");
		String message = "";
		
		if(!inputEmail.isEmpty()){
			Member classMember = new Member();
			Row memberRow = classMember.getMemberByEmailOnly(inputEmail);
			if(memberRow==null || memberRow.getSeqID()==null || memberRow.getSeqID().isEmpty()){
				message = "<span class='error'>"+this.gs.PROFILE_EMAIL+"を お確かめ下さい。</span>";
			}else{
				String memberID = memberRow.getSeqID();
				String nickName = memberRow.get("nickName").isEmpty()? "ユーザさん" : memberRow.get("nickName")+"さん";
				String subject = "【"+this.gs.NAME_SERVICE+"】"+this.gs.PROFILE_PASSWORD + "変更用URLのお知らせ";
				
				Reminder classReminder = new Reminder();
				String hashID = classReminder.newReminder(memberID);
	    		StringBuffer callbackURL = this.req.getRequestURL();  
	    		int index = callbackURL.lastIndexOf("/");  
	    		callbackURL.replace(index, callbackURL.length(), "");
	    		String servicePath = callbackURL.toString();	    		
				String br = "\n";
				String msgBody = "";
				msgBody += nickName + br;
				msgBody += br;
				msgBody += "下記URLより"+this.gs.PROFILE_PASSWORD+"を変更してください。"+br;
				msgBody += servicePath + "/login/passchg/" + hashID+br;
				msgBody += br;
				msgBody += "上記URLは、" + classReminder.expireTime + "経過後、無効になります。"+br;
				msgBody += "お早めに変更をお願いいたします。"+br;
				msgBody += br;
				msgBody += "※このメールにお心当たりのない場合は、URLにアクセスせずメールを破棄してください。";
				
				SendMail classSendMail = new SendMail();
				if(classSendMail.send(memberRow.get("email"), nickName, subject, msgBody)){				
					message = "<span class='success'>"+this.gs.PROFILE_PASSWORD+"再設定のためのURLを送信しました。</span>";
				}else{
					String deletedRowMemberID = null;
					while(deletedRowMemberID==null){
						Reminder toDeleteReminder = new Reminder();
						deletedRowMemberID = toDeleteReminder.getMemberIDbyHash(hashID);
					}
					message = "<span class='error'>ご指定のアドレスには、メールを送信できません。</span>";
				}
			}
		}
		
		String output = "";
		output += "<div style='text-align:center;'>";
		output += "<span class='info'>";
		output += "外部サービスのIDを使っていない場合に限り、"+this.gs.PROFILE_PASSWORD+"再設定のためのURLをメールで送信します。";
		output += "</span>";
		output += "<br><br>";
		output += "<form name='reminder' method='post' action='/login/reminder'>";
		output += "<span class='input_label'>" + this.gs.PROFILE_EMAIL + "：</span>";
		output += "<input id='email' name='email' type='text' style='ime-mode:disabled; margin-right:2em;' value='"+inputEmail+"'>";
		output += this.gs.getPsudoButton("document.reminder.submit();", "メール送信");
		output += "</form>";
		if(!message.isEmpty()){
			output += "<br><br>" + message;
		}
		output += "</div>";
		return output;
	}
}
