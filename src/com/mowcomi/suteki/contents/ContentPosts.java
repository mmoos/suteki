package com.mowcomi.suteki.contents;

import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.data.Post;
import com.mowcomi.suteki.data.Favorite;
import com.mowcomi.suteki.data.Comment;
import com.mowcomi.suteki.data.Popular;
import com.mowcomi.suteki.global.PageMenu;
import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentPosts extends Content{
	private String controller;
	private String baseURI = "/";
	private String page = "1";
	private String targetMemberID;
	private String viewMemberID;
	private LinkedList<String> favoritePosts;
	
	private int maxPage;
	private int limit = 20;
	
	private PageMenu pageMenu = new PageMenu();
	
	public ContentPosts(HttpServletRequest req, HttpServletResponse resp,
			String controller, LinkedHashMap<String,String> params, String viewMemberID){
		super(req,resp);
		String[] css = {"popup","posts"};
		this.setCss(css);
		String[] js = {"jquery-ui-1.8.16.custom.min", "textareaInsert", "popUpFavorite", "popUpComment"};
		this.setJs(js);

		this.controller = controller;
		this.baseURI += controller;
		if(params.containsKey("page")){ this.page = params.get("page"); }
		if(params.containsKey("targetMemberID")){ this.targetMemberID = params.get("targetMemberID"); }
		if(viewMemberID!=null && !viewMemberID.isEmpty()){ this.setViewMember(viewMemberID);}
	}
	
	public void setViewMember(String memberID){
		this.viewMemberID = memberID;		
		Favorite classFavorite = new Favorite();
		this.favoritePosts = classFavorite.getList("postID", memberID);
	}
	
	@Override
	public String getOutPut(){
		String output = "";
		LinkedHashMap<String,Row> posts;
		Post classPost = new Post();
		boolean showNewPost = false;
				
		if(this.controller.equals("popular")){
			Popular classPopular = new Popular();
			classPopular.setLimit(this.limit);
			classPopular.setPage(this.page);
			LinkedList<String> postIdList = classPopular.getPopularList();
			posts = classPost.getPostsByID(postIdList);
			this.maxPage = classPopular.getMaxPage();
		}else if(this.controller.equals("post_own")){
			showNewPost = true;
			classPost.setLimit(this.limit);
			classPost.setPage(this.page);
			posts = classPost.getPostsByMember(this.viewMemberID);
			this.maxPage = classPost.getMaxPage();
		}else if(this.controller.equals("post_other")){
			classPost.setLimit(this.limit);
			classPost.setPage(this.page);
			posts = classPost.getPostsByMember(this.targetMemberID);
			this.maxPage = classPost.getMaxPage();
		}else if(this.controller.equals("favorite_own")){
			Favorite classFavorite = new Favorite();
			classFavorite.setLimit(this.limit);
			classFavorite.setPage(this.page);
			LinkedList<String> postIdList = classFavorite.getList("postID", this.viewMemberID);
			posts = classPost.getPostsByID(postIdList);
			this.maxPage = classFavorite.getMaxPage();
		}else if(this.controller.equals("favorite_other")){
			Favorite classFavorite = new Favorite();
			classFavorite.setLimit(this.limit);
			classFavorite.setPage(this.page);
			LinkedList<String> postIdList = classFavorite.getList("postID", this.targetMemberID);
			posts = classPost.getPostsByID(postIdList);
			this.maxPage = classFavorite.getMaxPage();
		}else{
			showNewPost = true;
			classPost.setLimit(this.limit);
			classPost.setPage(this.page);
			posts = classPost.getPosts();
			this.maxPage = classPost.getMaxPage();	
		}
		
		if(showNewPost){
			ContentEditPost classEditPost = new ContentEditPost(this.req, this.resp, null, this.viewMemberID);
			output += classEditPost.getOutPut();
		}
		
		if(posts==null || posts.isEmpty()){
			if(!this.page.equals("1")){
				try{
					this.resp.sendRedirect(this.baseURI);
				}catch(Exception e){
					output += "<div class='empty'>まだ"+this.gs.NAME_POST+"がありません。</div>";
				}
			}else{
				output += "<div class='empty'>まだ"+this.gs.NAME_POST+"がありません。</div>";
			}
		}else{
			output += this.gs.getPaging(this.baseURI, this.maxPage, this.page, 3);
			for(Iterator<String> i=posts.keySet().iterator(); i.hasNext();){
				String key   = i.next().toString();
				output += this.getPostOne(key, posts.get(key));
			}
			output += this.gs.getPaging(this.baseURI, this.maxPage, this.page, 3);
		}
		return output;
	}
	
	private String getMemberInfo(String memberID){
		String memberInfo;
		String nickName = this.gs.getNickName(memberID);
		if(  memberID!=null && !memberID.isEmpty() && !memberID.equals(this.viewMemberID) ){
			memberInfo = "<div class='who'>"
					+ "BY:"
					+ "<a href='" + this.pageMenu.getLinkProfileOther(memberID) + "'>"
					+ nickName 
					+ "</a>"
					+ "</div>";
			memberInfo += "<div class='to_post'>"
					+ "<a href='" + this.pageMenu.getLinkPostOther(memberID) + "'>"
					+ ">>"+ this.gs.ACT_POST + "した" + this.gs.NAME_POST
					+ "</a>"
					+ "</div>";
			memberInfo += "<div class='to_favorite'>"
					+ "<a href='" + this.pageMenu.getLinkFavoriteOther(memberID) + "'>"
					+ ">>" + this.gs.ACT_FAVORITE + "している" + this.gs.NAME_POST
					+ "</a>"
					+ "</div>";
		}else{
			memberInfo = "<div class='who'>BY:" + nickName + "</div>";
		}
		return memberInfo;
	}
	
	private String getCommentOne(String commentID, Row comment){
		String memberInfo;
		String byMemberID = comment.get("memberID");
		if( byMemberID==null || byMemberID.isEmpty() ){
			memberInfo = this.getMemberInfo("");
		}else{
			memberInfo = this.getMemberInfo(byMemberID);
		}
		
		String commentOne = "";
		commentOne += "<div class='comment'>";
		commentOne += "<div class='head'>";
		commentOne += "<div class='time'>投稿日：" + comment.getCreatedAt() + "</div>";
		commentOne += memberInfo;
		commentOne += "</div>";
		commentOne += "<div class='content'>";
		commentOne += comment.get("content");
		commentOne += "</div>";
		commentOne += "</div>";
		return commentOne;
	}
	
	private String getPostOne(String postID, Row post){
		String byMemberID = "";
		String memberInfo;
		String editLink = "";
		String favoriteLink = "";
		String popular = "";
		Favorite classFavorite = new Favorite();
		LinkedList<String> favoriteMember = classFavorite.getList("memberID", postID);
		Comment classComment = new Comment();
		LinkedHashMap<String,Row> comments = classComment.searchCommentByPost(postID);
		int commentNum = comments==null ? 0 : comments.size();

		if( post.get("memberID")==null || post.get("memberID").isEmpty() ){
			memberInfo = this.getMemberInfo("");
		}else{
			byMemberID = post.get("memberID");
			memberInfo = this.getMemberInfo(byMemberID);
		}
		
		if( !byMemberID.isEmpty() && byMemberID.equals(this.viewMemberID) ){
			String editOnClick = "location.href=\""+this.pageMenu.getLinkEdit(postID)+"\"";
			String editText = this.gs.ACT_EDIT;
			editLink = "<div class='edit'>";
			editLink += this.gs.getPsudoButton(editOnClick, editText);
			editLink += "</div>";
		}else{
			favoriteLink = "<div class='favorite'>";
			if(this.favoritePosts!=null && this.favoritePosts.contains(postID)){
				favoriteLink += "この" + this.gs.NAME_POST + "に";
				favoriteLink += this.gs.ACT_FAVORITE + "しています";
			}else if(post.get("memberID")==null || !post.get("memberID").equals(this.viewMemberID)){
				String favoriteOnClick = "location.href=\"/servlet/favorite/"+postID+"\"";
				String favoriteText = this.gs.ACT_FAVORITE+"！";
				favoriteLink += this.gs.getPsudoButton(favoriteOnClick, favoriteText);
			}
			favoriteLink += "</div>";
		}
		
		if(favoriteMember!=null && !favoriteMember.isEmpty()){
			int favoriteNum = favoriteMember.size();
			
			String firstMemberID = favoriteMember.get(0);
			if( !firstMemberID.equals(this.viewMemberID) ){
				popular += "<a href='" + this.pageMenu.getLinkProfileOther(firstMemberID) + "'>";
				popular += this.gs.getNickName(firstMemberID);
				popular += "</a>";
			}
			favoriteNum --;
			
			if(favoriteNum > 0){
				String secondMemberID = favoriteMember.get(1);
				if( !secondMemberID.equals(this.viewMemberID) ){
					popular += "<a href='" + this.pageMenu.getLinkProfileOther(secondMemberID) + "'>";
					popular += this.gs.getNickName(secondMemberID);
					popular += "</a>";
				}
				favoriteNum --;	
			}
			
			if(favoriteNum > 0){
				if( favoriteNum==1 && favoriteMember.get(2).equals(this.viewMemberID) ){
					// 他１人は自分
				}else{
					popular += "<span class='psudo_a' onClick='popUpFavorite(\""+postID+"\");'>";
					popular += "他"+String.valueOf(favoriteNum)+"人";
					popular += "</span>";
				}
			}
		}
		if( !popular.isEmpty() ){
			popular = "<div class='popular'>" + popular + "が"+this.gs.ACT_FAVORITE+"しています</div>";
		}
		
		String postOne = "";
		postOne += "<div class='post'>";
		postOne += "<div class='head'>";
		postOne += memberInfo;
		postOne += "<div class='time'>投稿日：" + post.getCreatedAt() + "</div>";
		postOne += "</div>";
		postOne += "<div class='content'>";
		postOne += post.get("content");
		postOne += "</div>";
		postOne += "<div class='action'>";
		postOne += editLink;
		postOne += favoriteLink;
		postOne += popular;
		String commentOnClick = "popUpComment(\""+postID+"\", \""+commentNum+"\");";
		String commentText = this.gs.ACT_COMMENT+"する";
		postOne += "<div class='add_comment'>"+this.gs.getPsudoButton(commentOnClick, commentText)+"</div>";
		
		postOne += "</div>";
		postOne += "</div>";
		
		if(comments!=null && !comments.isEmpty()){
			for(Iterator<String> i = comments.keySet().iterator(); i.hasNext();){
				String commentID = i.next();
				Row comment = comments.get(commentID);
				postOne += this.getCommentOne(commentID, comment);
			}
		}
		return postOne;
	}
}
