package com.mowcomi.suteki.contents;

import com.mowcomi.suteki.global.GlobalStrings;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Content {
	public GlobalStrings gs = new GlobalStrings();
	public HttpServletRequest req;
	public HttpServletResponse resp;
	private String[] css;
	private String[] js;
	public String DELEMETER = "@@@";
	
	public Content(HttpServletRequest req, HttpServletResponse resp){
		this.req = req;
		this.resp = resp;
	}
	
	public void setCss(String[] css){ this.css = css; }
	public void setJs(String[] js){ this.js = js; }
	
	public String getJoined(String[] src){
		String returnStr = "";
		String and = "";
		for(int i=0; i<src.length; i++){
			returnStr += and + src[i];
			and = this.DELEMETER;
		}
		return returnStr;		
	}
	
	public String getCss(){
		return this.getJoined(this.css);
	}
	public String getJs(){
		return this.getJoined(this.js);
	}
	
	public String getOutPut(){ return ""; }
}
