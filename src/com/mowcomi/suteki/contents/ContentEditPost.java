package com.mowcomi.suteki.contents;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.data.Post;

public class ContentEditPost extends Content{
	private String postID;
	private String memberID;
	
	public ContentEditPost(HttpServletRequest req, HttpServletResponse resp,
			String postID, String memberID){
		super(req,resp);
		String[] css = {"popup","posts"};
		this.setCss(css);
		String[] js = {"jquery-ui-1.8.16.custom.min", "textareaInsert"};
		this.setJs(js);
		
		this.postID = postID;
		this.memberID = memberID;
	}
	
	public boolean checkEditable(Row oldPost){
		if(oldPost.get("memberID")==null || oldPost.get("memberID").isEmpty()){
			return false;
		}else if( !oldPost.get("memberID").equals(this.memberID) ){
			return false;
		}else{
			return true;
		}
	}
	
	@Override
	public String getOutPut(){
		if( this.postID!=null && !this.postID.isEmpty() ){
			Post classPost = new Post();
			Row oldPost = classPost.getByID(this.postID);
			if(!this.checkEditable(oldPost)){
				this.postID = "";
			}
		}else{
			this.postID = "";
		}
		
		String output = "";
		output += "<div id='textarea_load'></div>";
		output += "\n<script type='text/javascript'><!--\n";
		output += "jQuery('#textarea_load').load('/load/textarea/p_"+this.postID+"',function(){resizeImg();});";
		output += "\n--></script>\n";
		return output;
	}
}
