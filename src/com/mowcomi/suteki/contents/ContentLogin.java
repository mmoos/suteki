package com.mowcomi.suteki.contents;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentLogin extends Content{
	public ContentLogin(HttpServletRequest req, HttpServletResponse resp){
		super(req,resp);
		String[] css = {"login"};
		this.setCss(css);
		String[] js = new String[0];
		this.setJs(js);
	}
	
	@Override
	public String getOutPut(){
		String[][] openIdProviders = this.gs.getOpenIdProviders();
		
		String output = "";
		output += "<div id='login_normal'>";
		output += "<form name='login' method='post' action='/servlet/login'>";
		output += "<table id='login_table'>";
		output += "<th class='input_label'>" + this.gs.PROFILE_EMAIL + "：</th>";
		output += "<td><input id='email' name='email' type='text' style='ime-mode:disabled;'></td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th class='input_label'>" + this.gs.PROFILE_PASSWORD + "：</th>";
		output += "<td><input id='password' name='password' type='password'></td>";
		output += "</tr>";
		output += "<tr>";
		output += "<td colspan='2' align='center'>";
		output += "<input type='checkbox' name='stay' value='1' checked='checked'>";
		output += "<span class='small'>ログアウトしない</span>";
		output += this.gs.getPsudoButton("document.login.submit();", "ログイン");
		output += "</td>";
		output += "</tr>";
		output +="</table>";
		output +="</form>";
		output += "<div align='center'>";
		output += "<a href='/login/reminder'>"+this.gs.PROFILE_PASSWORD+"を忘れた</a>";
		output += "<br>";
		output += "<a href='/login/register' class='large'>新規アカウント作成</a>";
		output += "</div>";
		output += "</div>";
		output += "<div id='login_openid'>";
		output += "外部サービスのIDを使ってログインする<br>";
		for(int i=0; i<openIdProviders.length; i++) {
			output += "<a href='/servlet/openid/" + openIdProviders[i][1] + "'>";
			output += "<img src='/image/" + openIdProviders[i][0] +".png'>";
			output += "</a>";
		}
		output += "</div>";
		return output;
	}
}