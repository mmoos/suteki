package com.mowcomi.suteki.contents;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mowcomi.suteki.global.Profile;

public class ContentRegister extends Content{
	public ContentRegister(HttpServletRequest req, HttpServletResponse resp){
		super(req,resp);
		String[] css = {"popup","profile"};
		this.setCss(css);
		String[] js = {"jquery-ui-1.8.16.custom.min", "popUpPrefecture", "jquery.validate.min", "profileValidate"};
		this.setJs(js);
	}
	
	@Override
	public String getOutPut(){
		Profile classProfile = new Profile();
		String output = "";
		
		output += "<form id='form_profile' class='validate' name='form_profile' method='post' action='/servlet/profile'>";
		output += "<input type='hidden' name='from' value='register'>";
		output += "<table id='register'>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_NICKNAME + "</th>";
		output += "<td>" + classProfile.getProfileInput("nickName") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_EMAIL + "</th>";
		output += "<td>" + classProfile.getProfileInput("email") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_PASSWORD + "</th>";
		output += "<td>" + classProfile.getProfileInput("password") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_BIRTHDAY + "</th>";
		output += "<td>" + classProfile.getProfileInput("birthday") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_SEX + "</th>";
		output += "<td>" + classProfile.getProfileInput("sex") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_HOME + "</th>";
		output += "<td>" + classProfile.getProfileInput("home") + "</td>";
		output += "</tr>";
		output += "<tr>";
		output += "<th>" + this.gs.PROFILE_WEBSITE + "</th>";
		output += "<td>" + classProfile.getProfileInput("website") + "</td>";
		output += "</tr>";
		output += "</table>";
		output += "<div style='text-align:center; margin-top:1em;'>";
		String onClick = "if(jQuery(\"#form_profile\").valid()){document.form_profile.submit();}";
		String text = "登録する";
		output += this.gs.getPsudoButton(onClick, text);
		output += "</div>";
		output += "</form>";
		return output;
	}
}
