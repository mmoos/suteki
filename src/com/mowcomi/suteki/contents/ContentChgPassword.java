package com.mowcomi.suteki.contents;

import com.mowcomi.suteki.data.Reminder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentChgPassword extends Content{
	public ContentChgPassword(HttpServletRequest req, HttpServletResponse resp){
		super(req,resp);
		String[] css = {"login","profile"};
		this.setCss(css);
		String[] js = {"jquery.validate.min", "profileValidate"};
		this.setJs(js);
	}
	
	@Override
	public String getOutPut(){
		String hashID = this.req.getParameter("params");
		if(hashID==null || hashID.isEmpty()){
			return this.showError();
		}
		
		Reminder classReminder = new Reminder();
		if(classReminder.checkExpire(hashID)){
			return this.showTry(hashID);
		}else{
			return this.showError();
		}
	}
	
	private String showError(){
		Reminder classReminder = new Reminder();
		String output = "";
		output += "<div align='center'>";
		output += "<span class='error'>有効期限が過ぎています</span>";
		output += "<br><br>";
		output += "<span class='info'>";
		output += this.gs.PROFILE_PASSWORD+"再設定の期限は、メール送信から"+classReminder.expireTime+"です。";
		output += "</span>";
		output += "</div>";
		return output;
	}
	
	private String showTry(String hashID){
		String output = "";
		output += "<div style='text-align:center;'>";
		output += "<span class='info'>";
		output += "新しいパスワードを設定してください。";
		output += "</span>";
		output += "<br>";
		output += "<form id='chgpass' class='validate' name='chgpass' method='post' action='/servlet/chg_pass'>";
		output += "<input type='hidden' name='hash' value='"+hashID+"'>";
		output += "<table id='pass_chg_table'>";
		output += "<th style='width:10em;'>新しい" + this.gs.PROFILE_PASSWORD + "：</th>";
		output += "<td style='width:20em;'>";
		output += "<input type='password' id='password1' name='password1'>";
		output += "<label class='invalid' for='password1' generated='true' style='display:none;'></label>";
		output += "</td>";
		output += "</tr><tr>";
		output += "<th>確認：</th>";
		output += "<td>";
		output += "<input type='password' id='password2' name='password2'>";
		output += "<label class='invalid' for='password2' generated='true' style='display:none;'></label>";
		output += "</td>";
		output += "</tr>";
		output += "</table>";
		String onClick = "if(jQuery(\"#chgpass\").valid()){document.chgpass.submit();}";
		String text = "変更する";
		output += this.gs.getPsudoButton(onClick, text);
		output += "</form>";
		output += "</div>";
		return output;
	}
}
