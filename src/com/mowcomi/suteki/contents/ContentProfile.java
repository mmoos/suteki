package com.mowcomi.suteki.contents;

import com.mowcomi.suteki.global.PageMenu;
import com.mowcomi.suteki.global.Profile;
import com.mowcomi.suteki.global.Time;
import com.mowcomi.suteki.data.Member;
import com.mowcomi.suteki.row.Row;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentProfile extends Content{
	private String targetMemberID;
	private String viewMemberID;
	private boolean self;
	
	private PageMenu pm = new PageMenu();
	private Member classMember = new Member();
	private Profile classProfile = new Profile();
	
	public ContentProfile(HttpServletRequest req, HttpServletResponse resp,
			String targetMemberID, String viewMemberID ){
		super(req,resp);
		String[] css = {"popup","profile"};
		this.setCss(css);
		String[] js = {"jquery-ui-1.8.16.custom.min", "popUpPrefecture", "jquery.validate.min", "profileValidate"};
		this.setJs(js);
		
		this.targetMemberID = targetMemberID;
		this.viewMemberID = viewMemberID;
	}
	
	private boolean checkShow(Row memberData, String column){
		if( memberData.get("show_"+column)==null ){
			return true;
		}
		if( memberData.get("show_"+column).isEmpty() ){
			return false;
		}
		if(memberData.get("show_"+column).equals(this.classMember.ALWAYS_SHOW)){
			return true;
		}
		if(memberData.get("show_"+column).equals(this.classMember.NEVER_SHOW)){
			return false;
		}
		return false;
	}
	
	private String getTd(String column, Row memberData){
		String returnStr = "";
		if(this.self){
			if(this.classProfile.getProfileInput(column).isEmpty()){
				returnStr = "---";
			}else{
				returnStr += "<form id='form_"+column+"' class='validate' name='form_"+column+"' method='post' action='/servlet/profile'>";
				returnStr += "<input type='hidden' name='from' value='profile'>";
				returnStr += this.classProfile.getProfileInput(column);
				String onClick = "if(jQuery(\"#form_"+column+"\").valid()){document.form_"+column+".submit();}";
				String text = gs.ACT_EDIT;
				returnStr += "</td><td style='width:3em;'>" + this.gs.getPsudoButton(onClick,text);
				returnStr += "</form>";
			}
		}else{
			if( memberData.get(column)==null ){
				returnStr = "";
			}else if( !this.checkShow(memberData, column) ){
				returnStr = "ヒミツ";
			}else{
				if(column.equals("birthday")){
					String birthday = memberData.get(column);
					int birthYear = Integer.parseInt(birthday.substring(0,4));
					int birthMonthDay = Integer.parseInt(birthday.substring(4,8));
					Time classTime = new Time();
					int thisYear = Integer.parseInt(classTime.getTimeString("yyyy", 0));
					int thisMonthDay = Integer.parseInt(classTime.getTimeString("MMdd", 0));
					int age;
					if( thisMonthDay < birthMonthDay ){
						age = thisYear - birthYear - 1;
					}else{
						age = thisYear - birthYear;
					}
					returnStr = String.valueOf(age) + "歳";
				}else if(column.equals("website")){
					returnStr = "<a href='"+memberData.get(column)+"'>";
					returnStr += "<img src='http://favicon.qfor.info/w/" + memberData.get(column) + "'>";
					returnStr += memberData.get(column);
					returnStr += "</a>";
				}else{
					returnStr = memberData.get(column);
				}
			}
			returnStr = "<span class='other'>" + returnStr + "</span>";
		}
		return returnStr;
	}
	
	@Override
	public String getOutPut(){
		if(this.targetMemberID==null || this.targetMemberID.isEmpty()){
			try{
				this.resp.sendRedirect("");
			}catch(Exception e){
				return "";
			}
		}
				
		if(this.viewMemberID==null){
			this.self = false;
		}else if( this.viewMemberID.isEmpty() ){
			this.self = false;
		}else if( this.viewMemberID.equals(this.targetMemberID)){
			this.self = true;
			this.classProfile.setMemberData(this.targetMemberID);
		}else{
			this.self = false;
		}
		
		String output = "";
		if(!self){
			output += "<div class='profile_link'>";
			output += "<a href='" + this.pm.getLinkPostOther(this.targetMemberID) + "'>"
					+ this.gs.getNickName(this.targetMemberID) + "が"
					+ this.gs.ACT_POST + "した" + this.gs.NAME_POST
					+ "</a>";
			output += "<a href='" + this.pm.getLinkFavoriteOther(this.targetMemberID) + "'>"
					+ this.gs.getNickName(this.targetMemberID) + "が"
					+ this.gs.ACT_FAVORITE + "している" + this.gs.NAME_POST
					+ "</a>";
			output += "</div>";
		}
		
		Row memberData = this.classMember.getMemberByID(this.targetMemberID);
		
		output += "<table id='profile'>";
		output += "<tr>";
		output += "<th>"+this.gs.PROFILE_NICKNAME+"</th>";
		output += "<td>"+this.getTd("nickName", memberData)+"</td>";
		output += "</tr><tr>";
		output += "<th>"+this.gs.PROFILE_EMAIL+"</th>";
		output += "<td>"+this.getTd("email", memberData)+"</td>";
		output += "</tr><tr>";
		if(self){
			output += "<th>"+this.gs.PROFILE_BIRTHDAY+"</th>";
		}else{
			output += "<th>"+this.gs.PROFILE_AGE+"</th>";
		}
		output += "<td>"+this.getTd("birthday",memberData)+"</td>";
		output += "</tr><tr>";
		output += "<th>"+this.gs.PROFILE_SEX+"</th>";
		output += "<td>"+this.getTd("sex",memberData)+"</td>";
		output += "</tr><tr>";
		output += "<th>"+this.gs.PROFILE_HOME+"</th>";
		output += "<td>"+this.getTd("home",memberData)+"</td>";
		output += "</tr><tr>";
		output += "<th>"+this.gs.PROFILE_WEBSITE+"</th>";
		output += "<td>"+this.getTd("website",memberData)+"</td>";		
		output += "</tr>";
		output += "</table>";
		return output;
	}
}
