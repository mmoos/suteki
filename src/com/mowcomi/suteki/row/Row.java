package com.mowcomi.suteki.row;

import com.mowcomi.suteki.global.Time;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public class Row {
	@PrimaryKey @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY) public String seqID;	
	@Persistent public String created_at;
	@Persistent	public String updated_at;	
	
	public Row(String seqID, String created_at, String updated_at){
		this.setSeqID(seqID);
		this.setCreatedAt(created_at);
		this.setUpdatedAt(updated_at);
	}
	
	public String getSeqID(){ return this.seqID; }
	public void setSeqID(String seqID){ this.seqID = seqID; }
	
	public String getCreatedAt() { return created_at; }
	public void setCreatedAt(String created_at){ this.created_at =created_at;}
	public void setCreatedAt(int addSecond) {
		Time classTime = new Time();
		this.created_at = classTime.getTimeString("yyyy/MM/dd HH:mm:ss", addSecond);
	}
	
	public String getUpdatedAt() { return updated_at; }
	public void setUpdatedAt(String updated_at){ this.updated_at = updated_at; }
	public void setUpdatedAt(int addSecond) {
		Time classTime = new Time();
		this.updated_at = classTime.getTimeString("yyyy/MM/dd HH:mm:ss", addSecond);
	}
	
	public void set(String column, String value){}
	public String get(String column){ return null; }
	
	public String[] getColumns(){ return null; }
	
	@Override
	public String toString(){
		String[] columns = this.getColumns();
		String dump = this.getClass().getSimpleName();
		dump += "&nbsp;[";
		dump += "<br>&nbsp;&nbsp;&nbsp;&nbsp;seqID&nbsp;:&nbsp;"+this.getSeqID();
		dump += "<br>&nbsp;&nbsp;&nbsp;&nbsp;created_at&nbsp;:&nbsp;"+this.getCreatedAt();
		dump += "<br>&nbsp;&nbsp;&nbsp;&nbsp;updated_at&nbsp;:&nbsp;"+this.getUpdatedAt();
		for(int i=0; i<columns.length; i++){
			dump += "<br>&nbsp;&nbsp;&nbsp;&nbsp;"+columns[i]+"&nbsp;:&nbsp;"+this.get(columns[i]);
		}
		dump += "<br>]";
		return dump;
	}
}