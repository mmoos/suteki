package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class FavoriteRow extends Row{
	@Persistent public String memberID;
	@Persistent public String postID;
	
	public FavoriteRow( String seqID, String created_at, String updated_at,
			String memberID, String postID){
		super(seqID, created_at, updated_at);
		this.set("memberID", memberID);
		this.set("postID", postID);
	}

	@Override
	public void set(String key, String value){
		if(key.equals("memberID")){ this.memberID = value; }
		else if(key.equals("postID")){ this.postID = value; }
	}
	
	@Override
	public String get(String column){
		if(column.equals("memberID")){ return this.memberID;}
		else if(column.equals("postID")){ return this.postID; }
		else{ return null;}
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {"memberID", "postID"};
		return columns;
	}
}
