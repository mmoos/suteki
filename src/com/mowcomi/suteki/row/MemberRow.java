package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class MemberRow extends Row{
	@Persistent public String openUserId;
	@Persistent public String email;
	@Persistent	public String password;
	@Persistent	public String nickName;
	@Persistent public String birthday;
	@Persistent public String sex;
	@Persistent public String home;
	@Persistent public String website;
	@Persistent public String show_email;
	@Persistent public String show_birthday;
	@Persistent public String show_sex;
	@Persistent public String show_home;
	@Persistent public String show_website;
	
	public MemberRow( String seqID, String created_at, String updated_at,
			String openUserId, String email, String password, String nickName,
			String birthday, String sex, String home, String website,
			String show_email, String show_birthday, String show_sex, String show_home, String show_website){

		super(seqID, created_at, updated_at);
		this.set("openUserId", openUserId);
		this.set("email", email);
		this.set("password", password);
		this.set("nickName", nickName);
		this.set("birthday", birthday);
		this.set("sex", sex);
		this.set("home", home);
		this.set("website", website);
		this.set("show_email", show_email);
		this.set("show_birthday", show_birthday);
		this.set("show_sex", show_sex);
		this.set("show_home", show_home);
		this.set("show_website", show_website);
	}
	
	@Override
	public void set(String key, String value){
		if(key.equals("openUserId")){ this.openUserId = value; }
		else if(key.equals("email")){ this.email = value; }
		else if(key.equals("password")){ this.password = value; }
		else if(key.equals("nickName")){ this.nickName = value; }
		else if(key.equals("birthday")){ this.birthday = value; }
		else if(key.equals("sex")){ this.sex = value; }
		else if(key.equals("home")){ this.home = value; }
		else if(key.equals("website")){ this.website = value; }
		else if(key.equals("show_email")){ this.show_email = value; }
		else if(key.equals("show_birthday")){ this.show_birthday = value; }
		else if(key.equals("show_sex")){ this.show_sex = value; }
		else if(key.equals("show_home")){ this.show_home = value; }
		else if(key.equals("show_website")){ this.show_website = value; }
	}
	
	@Override
	public String get(String column){
		if(column.equals("openUserId")){ return this.openUserId;}
		else if(column.equals("email")){ return this.email; }
		else if(column.equals("password")){ return this.password; }
		else if(column.equals("nickName")){ return this.nickName; }
		else if(column.equals("birthday")){ return this.birthday; }
		else if(column.equals("sex")){ return this.sex; }
		else if(column.equals("home")){ return this.home; }
		else if(column.equals("website")){ return this.website; }
		else if(column.equals("show_email")){ return this.show_email; }
		else if(column.equals("show_birthday")){ return this.show_birthday; }
		else if(column.equals("show_sex")){ return this.show_sex; }
		else if(column.equals("show_home")){ return this.show_home; }
		else if(column.equals("show_website")){ return this.show_website; }
		else{ return null;}
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {
				"openUserId", "email", "password", "nickName", 
				"birthday", "sex", "home", "website",
				"show_email", "show_birthday", "show_sex", "show_home","show_website"
				};
		return columns;
	}
}