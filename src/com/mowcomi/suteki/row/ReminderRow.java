package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class ReminderRow extends Row{
	@Persistent public String memberID;
	@Persistent public String expire;
	
	public ReminderRow( String seqID, String created_at, String updated_at,
			String memberID, String expire){
		super(seqID, created_at, updated_at);
		this.set("memberID", memberID);
		this.set("expire", expire);
	}
	
	@Override
	public void set(String key, String value){
		if(key.equals("memberID")){ this.memberID = value; }
		else if(key.equals("expire")){ this.expire = value; }
	}
	
	@Override
	public String get(String column){
		if(column.equals("memberID")){ return this.memberID;}
		else if(column.equals("expire")){ return this.expire; }
		else{ return null; }
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {"memberID", "expire"};
		return columns;
	}
}