package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import com.google.appengine.api.datastore.Blob;

@PersistenceCapable
public class ImageRow extends Row{
	@Persistent
    private Blob blob;
	
	public ImageRow( String seqID, String created_at, String updated_at,
			byte[] buffer){
		super(seqID, created_at, updated_at);
		this.setImg(buffer);
	}
	
	public void setImg(byte[] buffer) {
        this.blob = new Blob(buffer);
    }
	
	public byte[] getImg() {
        return blob.getBytes();
    }
}