package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class CommentRow extends Row{
	@Persistent public String memberID;
	@Persistent public String postID;
	@Persistent public Text content;
	
	public CommentRow( String seqID, String created_at, String updated_at,
			String memberID, String postID, String content){
		super(seqID, created_at, updated_at);
		this.set("memberID", memberID);
		this.set("postID", postID);
		this.set("content", content);
	}
	
	@Override
	public void set(String key, String value){
		if(key!=null && value!=null){
			if(key.equals("memberID")){ this.memberID = value; }
			else if(key.equals("postID")){ this.postID = value; }
			else if(key.equals("content")){ 
				try{
					value = value.replaceAll("img src=\"js/", "img src=\"/js/");
				}catch(Exception e){				
				}
				this.content = new Text(value);
			}
		}
	}
	
	@Override
	public String get(String column){
		if(column.equals("memberID")){ return this.memberID;}
		else if(column.equals("postID")){ return this.postID; }
		else if(column.equals("content")){ return this.content.getValue(); }
		else{ return null; }
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {"memberID", "postID", "content"};
		return columns;
	}
}