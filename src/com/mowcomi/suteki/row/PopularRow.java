package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class PopularRow extends Row{
	@Persistent public String postID;
	@Persistent public String whole;
	@Persistent public String byMember;
	@Persistent public String byAnonymous;
	
	public PopularRow( String seqID, String created_at, String updated_at,
			String postID, String whole, String byMember, String byAnonymous){
		super(seqID, created_at, updated_at);
		this.set("postID", postID);
		this.set("whole", whole);
		this.set("byMember", byMember);
		this.set("byAnonymous", byAnonymous);
	}

	@Override
	public void set(String key, String value){
		if(key.equals("postID")){ this.postID = value; }
		else if(key.equals("whole")){ this.whole = value; }
		else if(key.equals("byMember")){ this.byMember = value; }
		else if(key.equals("byAnonymous")){ this.byAnonymous = value; }
	}
	
	@Override
	public String get(String column){
		if(column.equals("postID")){ return this.postID; }
		else if(column.equals("whole")){ return this.whole; }
		else if(column.equals("byMember")){ return this.byMember; }
		else if(column.equals("byAnonymous")){ return this.byAnonymous; }
		else{ return null;}
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {"postID", "whole", "byMember", "byAnonymous"};
		return columns;
	}
}