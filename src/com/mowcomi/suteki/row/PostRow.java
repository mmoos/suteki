package com.mowcomi.suteki.row;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable
public class PostRow extends Row{
	@Persistent public String memberID;
	@Persistent public Text content;
	
	public PostRow( String seqID, String created_at, String updated_at,
			String memberID, String content){
		super(seqID, created_at, updated_at);
		this.set("memberID", memberID);
		this.set("content", content);
	}
	
	@Override
	public void set(String key, String value){
		if(key.equals("memberID")){ this.memberID = value; }
		else if(key.equals("content")){ 
			try{
				value = value.replaceAll("img src=\"js/", "img src=\"/js/");
			}catch(Exception e){				
			}
			this.content = new Text(value);
		}
	}

	@Override
	public String get(String column){
		if(column.equals("memberID")){ return this.memberID;}
		else if(column.equals("content")){ return this.content.getValue(); }
		else{ return null;}
	}
	
	@Override
	public String[] getColumns(){
		String[] columns = {"memberID", "content"};
		return columns;
	}

}