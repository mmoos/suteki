package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import com.mowcomi.suteki.data.Image;
import com.mowcomi.suteki.row.ImageRow;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;


@SuppressWarnings("serial")
public class ImageServlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	String src = req.getParameter("params");
    	resp.setContentType("image/png");
        ServletOutputStream out = resp.getOutputStream();
        
    	Image classImage = new Image();
    	ImageRow row = classImage.getByID(src);
    	
    	if(row!=null){
	    	byte[] imgByte = row.getImg();
	        BufferedInputStream imgStream = new BufferedInputStream(new ByteArrayInputStream(imgByte));
	        int len;
	        while( (len = imgStream.read(imgByte, 0, imgByte.length)) > 0) {
	        	out.write(imgByte, 0, len);
	        }
    	}else{
    		out.write(null);
    	}
    	out.close();

    }
}