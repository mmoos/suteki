package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.Login;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	Login classLogin = new Login(req,resp);
    	classLogin.logout();
    	UserService userService = UserServiceFactory.getUserService();
    	resp.sendRedirect(userService.createLogoutURL(classLogin.getBackPage()));
    }
}
