package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Post;
import com.mowcomi.suteki.row.Row;
import com.mowcomi.suteki.contents.ContentEditPost;
import com.mowcomi.suteki.global.Login;
import com.mowcomi.suteki.global.PageMenu;

@SuppressWarnings("serial")
public class PostServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Login classLogin = new Login(req,resp);
    	boolean loggedIn = classLogin.tryLogin();

    	PageMenu classPageMenu = new PageMenu();
    	String def = loggedIn ? "own" : "new";
    	String backPage = classPageMenu.getBackPage(def, req.getSession());

    	Post classPost = new Post();
    	String content = req.getParameter("content");
    	String postID = req.getParameter("post_id");

    	if(postID!=null && !postID.isEmpty()){
    		Row oldPost = classPost.getByID(postID);
    		ContentEditPost classContentEditPost = new ContentEditPost(req,resp,postID,classLogin.getMemberID());
    		if(classContentEditPost.checkEditable(oldPost)){
    			classPost.editPost(postID, content);
    			resp.sendRedirect(backPage+"/edit_post");
    		}else{
    			resp.sendRedirect(backPage);
    		}
    	}else{
    		String newPostID = classPost.newPost(classLogin.getMemberID(), content);
    		Row newPost = null;
    		int cnt = 0;
    		while(newPost==null){
    			if(cnt>1000){ break; }
    			Post classPostToFind = new Post();
    			newPost = classPostToFind.getByID(newPostID);
    			try{
    				resp.wait(50);
    			}catch(Exception e){    				
    			}
    			cnt ++;
    		}
    		resp.sendRedirect("/new/new_post");
    	}
    }
}
