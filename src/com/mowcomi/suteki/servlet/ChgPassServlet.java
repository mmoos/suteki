package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Reminder;
import com.mowcomi.suteki.data.Member;
import com.mowcomi.suteki.global.Login;
import java.util.LinkedHashMap;

@SuppressWarnings("serial")
public class ChgPassServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	String errorPage = "/login/passchg";
    	
    	String hashID = req.getParameter("hash");
    	String password = req.getParameter("password1");
    	if(hashID==null || hashID.isEmpty() || password==null || password.isEmpty()){
    		resp.sendRedirect(errorPage);
    	}
    	
    	Reminder classReminder = new Reminder();
    	String memberID = classReminder.getMemberIDbyHash(hashID);
    	if(memberID==null || memberID.isEmpty()){
    		resp.sendRedirect(errorPage);
    	}
    	
    	LinkedHashMap<String,String> memberData = new LinkedHashMap<String,String>();
    	memberData.put("password", password);
    	Member classMember = new Member();
    	classMember.editProfile(memberID, memberData);
    	Login classLogin = new Login(req,resp);
    	resp.sendRedirect(classLogin.getLoginPage()+"/pass_changed");
   }
}