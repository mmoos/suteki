package com.mowcomi.suteki.servlet;

import java.io.IOException;
import java.util.LinkedHashMap;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Member;
import com.mowcomi.suteki.global.Login;

@SuppressWarnings("serial")
public class ProfileServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	if(req.getParameter("from")==null){
    		resp.sendRedirect("/");
    	}
    	
    	Member classMember = new Member();
    	Login classLogin = new Login(req,resp);
    	LinkedHashMap<String,String> memberData = new LinkedHashMap<String,String>();
    	String[] memberRowColumns = classMember.getNewRow().getColumns();
    	
    	for (int i=0; i<memberRowColumns.length; i++){
    		String column = memberRowColumns[i];
    		String value = null;
    		if(column.equals("password")){
    			value = req.getParameter("password1");
    		}else if(column.equals("birthday")){
    			if(req.getParameter("birth_year")!=null && !req.getParameter("birth_year").isEmpty()){
    				value = req.getParameter("birth_year") + req.getParameter("birth_month") + req.getParameter("birth_day");
    			}
    		}else{
    			value = req.getParameter(column);
    		}
    		if(value!=null){
    			memberData.put(column, value);
    		}
    		
    		if(req.getParameter(column+"_show")!=null){
    			memberData.put(column+"_show", req.getParameter(column+"_show"));
    		}
    	}
    	
    	if(req.getParameter("from").equals("register")){
        	classMember.newMember(memberData);
        	classLogin.setEmail(req.getParameter("email"));
        	classLogin.setPassword(req.getParameter("password1"));
        	classLogin.dontSaveCookie();
        	int cnt = 0;
        	while(!classLogin.loginWithEmail()){
        		if(cnt>1000){
        			resp.sendRedirect(classLogin.getLoginErrorPage());
        		}
        		try{
        			resp.wait(10);
        		}catch(Exception e){
        		}
        		cnt ++;
        	}
        	resp.sendRedirect(classLogin.getBackPage()+"/registered");
    	}else if(req.getParameter("from").equals("profile")){
    		if(!classLogin.tryLogin()){
    			resp.sendRedirect("/");
    		}else{
    			classMember.editProfile(classLogin.getMemberID(), memberData);
    			resp.sendRedirect("/profile_own/edit_profile");
    		}
    	}
    }
}