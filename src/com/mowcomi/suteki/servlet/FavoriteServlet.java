package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Favorite;
import com.mowcomi.suteki.data.Popular;
import com.mowcomi.suteki.global.Login;
import com.mowcomi.suteki.global.PageMenu;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class FavoriteServlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	Login classLogin = new Login(req,resp);
    	boolean loggedIn = classLogin.tryLogin();

    	PageMenu classPageMenu = new PageMenu();
    	String def = loggedIn ? "favorite" : "new";
    	String backPage = classPageMenu.getBackPage(def, req.getSession());
    	String postID = req.getParameter("params");

    	if(postID==null || postID.isEmpty()){
    		resp.sendRedirect(backPage);
    	}else{
    		if(loggedIn){
    			Favorite classFavoriteSearch = new Favorite();
    			LinkedList<String> already = classFavoriteSearch.getList("postID", classLogin.getMemberID());
    			if(already==null || !already.contains(postID) ){		
    				Favorite classFavorite = new Favorite();
    				classFavorite.newFavorite(classLogin.getMemberID(), postID);
    				Popular classPopular = new Popular();
    				classPopular.addCnt(postID, true);
    			}else{
    				resp.sendRedirect(backPage + "/already_favorite");
    			}
    		}else{
    			Popular classPopular = new Popular();
    			classPopular.addCnt(postID, false);
    		}
    		resp.sendRedirect(backPage+"/add_favorite");
    	}
    }
}
