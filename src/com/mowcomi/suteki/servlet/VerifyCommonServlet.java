package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.Login;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class VerifyCommonServlet extends HttpServlet {
	   public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Login classLogin = new Login(req,resp);
    	String backPage = classLogin.getBackPage();
    	String loginErrorPage = classLogin.getLoginErrorPage();

    	UserService userService = UserServiceFactory.getUserService();
    	User user = userService.getCurrentUser();
    	if(user==null){
    		resp.sendRedirect(loginErrorPage);
    	}else{
    		classLogin.setEmail(user.getEmail().toString());
			classLogin.setOpenUserId(user.getUserId());
			classLogin.setNickName(user.getNickname());
			classLogin.loginWithOpenID();
			resp.sendRedirect(backPage);
    	}
    }
}