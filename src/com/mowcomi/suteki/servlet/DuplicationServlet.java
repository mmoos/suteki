package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Member;

@SuppressWarnings("serial")
public class DuplicationServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	boolean isAvailable = false;
    	if(req.getParameter("column")!=null){
    		Member classMember = new Member();

    		if(req.getParameter("column").equals("email") 
    				&& req.getParameter("email")!=null
    				&& !req.getParameter("email").isEmpty()
    				){
    			isAvailable = classMember.isAvailableEmail(req.getParameter("email"));
    		}else if(req.getParameter("column").equals("nickName") 
    				&& req.getParameter("nickName")!=null
    				&& !req.getParameter("nickName").isEmpty()
    				){
    			isAvailable = classMember.isAvailableNickName(req.getParameter("nickName"));
    		}
    	}
    	resp.getWriter().print(isAvailable);
    	this.destroy();
    }
}
