package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.net.URLConnection;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import org.mozilla.universalchardet.UniversalDetector;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesServiceFactory;
  
@SuppressWarnings("serial")
public class ExternalServlet extends HttpServlet {
	private String getNeeded(String url) throws IOException{
		String needed = "";
		
		URL insURLencode = new URL(url);
		URLConnection ucEncode = insURLencode.openConnection();
		ucEncode.connect();
        byte[] bufEncode = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);
        int readEncode;
        while ((readEncode = ucEncode.getInputStream().read(bufEncode)) > 0 && !detector.isDone()) {
            detector.handleData(bufEncode, 0, readEncode);
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();

        URL insURLmain = new URL(url);
		URLConnection ucMain = insURLmain.openConnection();
		ucMain.connect();
        InputStreamReader isr = new InputStreamReader(ucMain.getInputStream(),encoding);
        BufferedReader br = new BufferedReader(isr);
        
        Pattern patternTitle = Pattern.compile("<title>.*</title>", Pattern.CASE_INSENSITIVE);
    	Pattern patternMeta = Pattern.compile("<meta[^>]*>", Pattern.CASE_INSENSITIVE);
    	Pattern patternImg = Pattern.compile("<\\s*img.*src\\s*=\\s*([\\\"'])?([^ \\\"']*)[^>]*>", Pattern.CASE_INSENSITIVE);
    	
    	String s = null;
        while ( (s = br.readLine()) != null) {
        	Matcher matcherTitle = patternTitle.matcher(s);
        	while(matcherTitle.find()){
        		needed += matcherTitle.group()+"\n";
        	}
        	
        	Matcher matcherMeta = patternMeta.matcher(s);
        	while(matcherMeta.find()){
        		needed += matcherMeta.group()+"\n";
        	}
        	
        	Matcher matcherImg = patternImg.matcher(s);
        	while(matcherImg.find()){
        		String imgSrc = matcherImg.group(2);
        		if(imgSrc.length()<4 || !imgSrc.substring(0,4).equals("http")){
        			imgSrc = "http://"+insURLmain.getHost()+imgSrc;
        		}
        		
        		URL insURLimg = new URL(imgSrc);
        		InputStream in = insURLimg.openStream();
        		byte[] bufImg = new byte[4096];
        		try{
        			in.read(bufImg);
        			Image img = ImagesServiceFactory.makeImage(bufImg);
        			int originalH = img.getHeight();
        			int originalW = img.getWidth();
        			int h=0, w=0;
        			if( originalH>originalW && originalH>100 && originalW>0){
        				h = 100;
        				w = 100 * originalW / originalH;
        			}else if( originalH<=originalW && originalW>100 && originalH>0){
        				h = 100 * originalH / originalW;
        				w = 100;
        			}else{
        				h = originalH;
        				w = originalW;
        			}
        			if( h>5 && w>5 ){
        				needed += "<img src='"+imgSrc+"' class='ext_img' height='"+h+"' width='"+w+"'>\n";
        			}
        		}catch(Exception e){}
        	}
        }
    	return needed;
	}
	
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	String url = req.getParameter("url");
    	String needed = this.getNeeded(url);
    	resp.setContentType("text/plain");
    	resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(needed);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	String url = req.getParameter("params");
    	String needed = this.getNeeded(url);
    	resp.setContentType("text/plain");
    	resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(needed);
    }
}