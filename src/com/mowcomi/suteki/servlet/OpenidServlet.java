package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.Login;
import com.mowcomi.suteki.global.TwitterSuteki;
import com.google.appengine.api.users.*;
import java.util.Set;
import java.util.HashSet;
import twitter4j.Twitter;
import twitter4j.auth.RequestToken;

@SuppressWarnings("serial")
public class OpenidServlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	String openIdServer = "";
    	Login classLogin = new Login(req,resp);
    	String loginErrorPage = classLogin.getLoginErrorPage();
    	
    	if(req.getParameter("params")==null || req.getParameter("params").isEmpty()){    		
    		resp.sendRedirect(loginErrorPage);
    	}else{
    		openIdServer = req.getParameter("params");
    		StringBuffer callbackURL = req.getRequestURL();  
    		int index = callbackURL.lastIndexOf("/");  
    		callbackURL.replace(index, callbackURL.length(), "");
    		String backURI = callbackURL.toString();
    		String providerLoginUrl = "";
    		if(openIdServer.equals("twitter")){
    			backURI += "/verify_twitter";
    			TwitterSuteki classTwitterSuteki = new TwitterSuteki();
    			Twitter twitter = classTwitterSuteki.getTwitterSuteki();
    			try{
    				RequestToken requestToken = twitter.getOAuthRequestToken(backURI);
        			req.getSession().setAttribute("requestToken", requestToken);
        			providerLoginUrl = requestToken.getAuthorizationURL();
    			}catch(Exception e){
    				resp.sendRedirect(loginErrorPage);
    			}
    		}else{
    			backURI += "/verify_common";
    			UserService userService = UserServiceFactory.getUserService();
    			Set<String> attributesRequest = new HashSet<String>();
    			attributesRequest.add("openid.mode=checkid_immediate");
    			attributesRequest.add("openid.ns=http://specs.openid.net/auth/2.0");
    			attributesRequest.add("openid.return_to=" + backURI);
    			providerLoginUrl = userService.createLoginURL(
    				backURI,
    	        	req.getServerName(),
    	        	openIdServer,
    	        	attributesRequest
    	        	);
    		}
    		resp.sendRedirect(providerLoginUrl);
    	}
    }
}
