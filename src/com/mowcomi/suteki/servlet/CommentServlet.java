package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.data.Comment;
import com.mowcomi.suteki.global.Login;
import com.mowcomi.suteki.global.PageMenu;

@SuppressWarnings("serial")
public class CommentServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	Login classLogin = new Login(req,resp);
    	classLogin.tryLogin();

    	PageMenu classPageMenu = new PageMenu();
    	String backPage = classPageMenu.getBackPage("new", req.getSession());

    	String postID = req.getParameter("post_id");  
    	if(postID==null || postID.isEmpty()){
    		resp.sendRedirect(backPage);
    	}

    	Comment classComment = new Comment();
    	String content = req.getParameter("content");
    	classComment.newComment(classLogin.getMemberID(), postID, content);
    	resp.sendRedirect(backPage+"/new_post");
    }
}