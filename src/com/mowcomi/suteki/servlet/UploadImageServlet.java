package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;

@SuppressWarnings("serial")
public class UploadImageServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	final int size = -1;
    	resp.setContentType("text/plain");
    	
    	ServletFileUpload upload = new ServletFileUpload();
		upload.setSizeMax(size);
		try{
			FileItemIterator iterator = upload.getItemIterator(req);
    	    while (iterator.hasNext()) {
    	    	FileItemStream item = iterator.next();
    	    	InputStream stream = item.openStream();
    	        
    	        if(!item.isFormField()){
    	        	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	        	byte[] buffer = new byte[4096];
    	        	int len=0;
    	        	while( (len = stream.read(buffer, 0, buffer.length)) != -1){
    	        		out.write(buffer, 0, len);    	                
    	        	}
    	        	
    	        	Image originalImg = ImagesServiceFactory.makeImage(out.toByteArray());
    	        	int originalH = originalImg.getHeight();
    	        	int originalW = originalImg.getWidth();
    	        	int h=0,w=0;
        			if( originalH>originalW && originalH>100 && originalW>0){
        				h = 100;
        				w = 100 * originalW / originalH;
        			}else if( originalH<=originalW && originalW>100 && originalH>0){
        				h = 100 * originalH / originalW;
        				w = 100;
        			}else{
        				h = originalH;
        				w = originalW;
        			}
		
        			Transform tf = ImagesServiceFactory.makeResize(w, h);
        			ImagesService is = ImagesServiceFactory.getImagesService();
        			Image resizedImg;
        			try{
        				resizedImg = is.applyTransform(tf, originalImg, ImagesService.OutputEncoding.PNG);
        			}catch(Exception e1){
        				try{
        					resizedImg = is.applyTransform(tf, originalImg, ImagesService.OutputEncoding.JPEG);
        				}catch(Exception e2){
        					resizedImg = is.applyTransform(tf, originalImg, ImagesService.OutputEncoding.WEBP);
        				}
        			}
    	        	com.mowcomi.suteki.data.Image classImage = new com.mowcomi.suteki.data.Image();
    	        	String src = classImage.newImage(resizedImg.getImageData());
    	            resp.getWriter().print(src);
    	        }
    	    }
		}catch(FileUploadException e){
			resp.getWriter().print("");
		}
    }
}
 