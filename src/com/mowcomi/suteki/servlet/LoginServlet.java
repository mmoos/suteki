package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.Login;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Login classLogin = new Login(req,resp);
    	String backPage = classLogin.getBackPage();
    	String loginErrorPage = classLogin.getLoginErrorPage();
    	
    	if(req.getParameter("email")==null){
    		resp.sendRedirect(loginErrorPage);
    	}else if(req.getParameter("email").isEmpty()){
    		resp.sendRedirect(loginErrorPage);
    	}else if(req.getParameter("password")==null){
    		resp.sendRedirect(loginErrorPage);
    	}else if(req.getParameter("password").isEmpty()){
    		resp.sendRedirect(loginErrorPage);
    	}else{
    		if(req.getParameter("stay")==null || req.getParameter("stay").isEmpty()){
    			classLogin.dontSaveCookie();
    		}
    		classLogin.setEmail(req.getParameter("email"));
    		classLogin.setPassword(req.getParameter("password"));
    		if(classLogin.loginWithEmail()){
    			resp.sendRedirect(backPage);
    		}else{
    			resp.sendRedirect(loginErrorPage);
    		}
    	}
	}
}
