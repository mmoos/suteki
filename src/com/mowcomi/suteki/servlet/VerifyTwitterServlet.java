package com.mowcomi.suteki.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mowcomi.suteki.global.Login;
import com.mowcomi.suteki.global.TwitterSuteki;
import twitter4j.*;
import twitter4j.auth.*;

@SuppressWarnings("serial")
public class VerifyTwitterServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
    	Login classLogin = new Login(req,resp);
    	String backPage = classLogin.getBackPage();
    	String loginErrorPage = classLogin.getLoginErrorPage();

    	String verifier;
    	if(req.getParameter("oauth_verifier")==null){
    		resp.sendRedirect(loginErrorPage);
    	}else{
    		verifier = req.getParameter("oauth_verifier");
    		TwitterSuteki classTwitterSuteki = new TwitterSuteki();
    		Twitter twitter = classTwitterSuteki.getTwitterSuteki();
    		RequestToken requestToken = (RequestToken) req.getSession().getAttribute("requestToken");  
    		AccessToken accessToken = null;
    		if(requestToken==null){
    			resp.sendRedirect(loginErrorPage);
    		}else{
    			try {
    				accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
    				classLogin.setNickName(accessToken.getScreenName());
    				classLogin.setOpenUserId("twitter_"+accessToken.getUserId());
    				classLogin.loginWithOpenID();
    				resp.sendRedirect(backPage);
    			} catch (Exception e) {
    				resp.sendRedirect(loginErrorPage);
    			}
    		}
    	}
    }
}
