<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.mowcomi.suteki.global.GlobalStrings" %>
<%@ page import="com.mowcomi.suteki.contents.Content" %>
<%@ page import="com.mowcomi.suteki.contents.ContentLogin" %>
<%@ page import="com.mowcomi.suteki.contents.ContentRegister" %>
<%@ page import="com.mowcomi.suteki.contents.ContentReminder" %>
<%@ page import="com.mowcomi.suteki.contents.ContentChgPassword" %>
<%@ page import="java.net.URLEncoder" %>
<%
GlobalStrings gs = new GlobalStrings();

String controller = request.getParameter("controller");
String pageTitle = "";
String extraMessage = "";
Content classContent;
if(controller==null){
	response.sendRedirect("/");
}
if(controller.equals("open")){
	pageTitle = "ログイン";
	if(request.getParameter("params")!=null){
		if(request.getParameter("params").equals("error")){
			extraMessage = "<div class='error'>ログインできませんでした</div>";
		}else if(request.getParameter("params").equals("pass_changed")){
			extraMessage = "<div class='success'>"+gs.PROFILE_PASSWORD+"を変更しました</div>";
		}
	}
	classContent = new ContentLogin(request,response);
}else if(controller.equals("register")){
	pageTitle = "新規アカウント登録";
	classContent = new ContentRegister(request,response);
}else if(controller.equals("reminder")){
	pageTitle = gs.PROFILE_PASSWORD+"を忘れた";
	classContent = new ContentReminder(request,response);
}else if(controller.equals("passchg")){
	pageTitle = gs.PROFILE_PASSWORD+"変更";
	classContent = new ContentChgPassword(request,response);
}else{
	response.sendRedirect("/");
	classContent = new Content(request,response);
}
String output = classContent.getOutPut();
String css = classContent.getCss();
String js = classContent.getJs();
String header = "/load/header.jsp?pageTitle="+URLEncoder.encode(pageTitle,"UTF-8")+"&css="+css+"&js="+js;
%>
<jsp:include page="<%= header %>" />
<div id='wrapper'>
<div id='page_title'><%= pageTitle %></div>
<%= extraMessage %>
<div id='content'>
<%= output %>
</div>
</div>
<jsp:include page="/load/footer.jsp" />