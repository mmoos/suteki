<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.mowcomi.suteki.global.GlobalStrings" %>
<%@ page import="com.mowcomi.suteki.global.PageMenu" %>
<%@ page import="com.mowcomi.suteki.global.Login" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="com.mowcomi.suteki.contents.Content" %>
<%@ page import="com.mowcomi.suteki.contents.ContentPosts" %>
<%@ page import="com.mowcomi.suteki.contents.ContentEditPost" %>
<%@ page import="com.mowcomi.suteki.contents.ContentProfile" %>
<%@ page import="java.net.URLEncoder" %>
<%
Login classLogin = new Login(request,response);
boolean loggedIn = classLogin.tryLogin();
String loginData;
if(loggedIn){
	GlobalStrings gs = new GlobalStrings();
	loginData = "こんにちは&nbsp;&nbsp;";
	loginData += gs.getNickName(classLogin.getMemberID());
	loginData += "&nbsp;&nbsp;";
	loginData += "<a href='" + classLogin.getLogoutPage() + "'>ログアウト</a>";
}else{
	loginData = "<a href='" + classLogin.getLoginPage() + "'>ログイン</a>";
}

PageMenu classPageMenu = new PageMenu();
classPageMenu.setLoginFlag(loggedIn);
classPageMenu.setControllerAndParams(request.getParameter("controller"), request.getParameter("params"));

String pageTitle = classPageMenu.getPageTitle();
String controller = classPageMenu.getController();
LinkedHashMap<String,String> params = classPageMenu.getParams();
String controllerToReturn = classPageMenu.getControllerToReturn();
String paramsToReturn = classPageMenu.getParamsToReturn();

session.setAttribute("controller",controllerToReturn);
session.setAttribute("params",paramsToReturn);

String extraMessage = "";
if(params.containsKey("extra")){
	extraMessage = params.get("extra");
}
String menu = classPageMenu.getMenu(controller);

Content classContent;
if(controller.equals("profile_own") || controller.equals("profile_other") ){
	String targetMemberID;
	if(controller.equals("profile_own")){
		targetMemberID = classLogin.getMemberID();
	}else{
		targetMemberID = params.get("targetMemberID");
	}
	classContent = new ContentProfile(request, response, targetMemberID, classLogin.getMemberID());
}else if(controller.equals("post")){
	String postID = params.containsKey("postID") ? params.get("postID") : null;
	classContent = new ContentEditPost(request, response,postID, classLogin.getMemberID());
}else{
	classContent = new ContentPosts(request, response, controller, params, classLogin.getMemberID());
}
String output = classContent.getOutPut();
String css = classContent.getCss();
String js = classContent.getJs();
String header = "/load/header.jsp?pageTitle="+URLEncoder.encode(pageTitle,"UTF-8")+"&css="+css+"&js="+js;
%>
<jsp:include page="<%= header %>" />
<div id='wrapper'>
<div id="login_data"><%= loginData %></div>
<div id='page_title'><%= pageTitle %></div>
<%= extraMessage %>
<div id="menu_list"><%= menu %></div>
<div id='content'>
<%= output %>
</div>
</div>
<jsp:include page="/load/footer.jsp" />