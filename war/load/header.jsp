<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.mowcomi.suteki.global.GlobalStrings" %>
<%@ page import="com.mowcomi.suteki.contents.Content" %>
<%
GlobalStrings gs = new GlobalStrings();
Content c = new Content(request,response);
String pageTitle = request.getParameter("pageTitle").toString();
String[] css = request.getParameter("css").toString().split(c.DELEMETER);
String[] js = request.getParameter("js").toString().split(c.DELEMETER);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<title><%= gs.NAME_SERVICE %>|<%= pageTitle %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/menu.css" />
<% for(int i=0; i<css.length; i++){ %>
<link rel="stylesheet" type="text/css" href="/css/<%= css[i] %>.css" /><% } %>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<% for(int i=0; i<js.length; i++){ %>
<script type="text/javascript" src="/js/<%= js[i] %>.js"></script><% } %>
<script type="text/javascript"><!--
jQuery(document).ready(function() {
	validateWhole();
	resizeImg();
});
--></script>
</head>
<body>
<div id='header'>
<span style='background:#ffffff; border:solid 2px #000000; padding: 0.4em;'>
<a href='/'>ロゴ</a>
</span>
</div>