<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.mowcomi.suteki.global.Login" %>
<%@ page import="com.mowcomi.suteki.global.GlobalStrings" %>
<%@ page import="com.mowcomi.suteki.global.PageMenu" %>
<%@ page import="com.mowcomi.suteki.data.Favorite" %>
<%@ page import="com.mowcomi.suteki.data.Member" %>
<%@ page import="java.util.LinkedList" %>
<%
Login classLogin = new Login(request,response);
boolean loggedIn = classLogin.tryLogin();
String viewMemberID = classLogin.getMemberID();

GlobalStrings gs = new GlobalStrings();
PageMenu pm = new PageMenu();

String postID = request.getParameter("params");

Favorite classFavorite = new Favorite();
LinkedList<String> members = classFavorite.getList("memberID",postID);
%>
<ul class='members'>
<%
String p = gs.ACT_POST + "した" + gs.NAME_POST;
String f = gs.ACT_FAVORITE + "した" + gs.NAME_POST;
int minus = 0;
for(int i=0; i<members.size(); i++){
	String memberID = members.get(i);
	if(memberID.equals(viewMemberID)){
		minus = 1;
	}else{
		String oddOrEven = (i-minus)%2==0 ? "even" : "odd";
%>
 <li>
	<div class="<%= oddOrEven %>">
	 <div class="member_who"><a href="<%= pm.getLinkProfileOther(memberID)%>"><%= gs.getNickName(memberID) %></a></div>
	 <div class="member_post"><a href="<%= pm.getLinkPostOther(memberID)%>"><%= p %></a></div>
	 <div class="member_fav"><a href="<%= pm.getLinkFavoriteOther(memberID)%>"><%= f %></a></div>
	</div>
 </li>
<%
	}
}
%>
</ul>