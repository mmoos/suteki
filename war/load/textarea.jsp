<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.mowcomi.suteki.data.Post" %>
<%@ page import="com.mowcomi.suteki.row.Row" %>
<%@ page import="com.mowcomi.suteki.global.GlobalStrings" %>
<%
GlobalStrings gs = new GlobalStrings();
String buttonText = gs.NAME_POST;

String id = request.getParameter("params");
String[] idInfo = id.split("_");
String action = "";
String old = "";
String hidden = "";
String postID = idInfo.length>1 ? idInfo[1]: "";
if(idInfo[0].equals("p")){
	action = "/servlet/post";
	if(postID!=null && !postID.isEmpty()){
		Post classPost = new Post();
		Row oldPost = classPost.getByID(postID);
		String oldContent = oldPost.get("content");
		if(oldContent!=null && !oldContent.isEmpty()){
			old = "<div class='old_post'>";
			old += "<div class='title'>元の投稿</div>";
			old += "<div class='old'>"+oldContent+"</div>";
			old += "</div>";
		}
		hidden = "<input type='hidden' name='post_id' value='"+postID+"'>";
	}
}else if(idInfo[0].equals("c")){
	action = "/servlet/comment";
	hidden = "<input type='hidden' name='post_id' value='"+postID+"'>";
}
%>
<div class='textarea_and_button' id='outer_<%= id %>'>
	<div class='textarea_buttons'>
		<span class='psudo_button' onClick='popupImg("<%= id %>");'>画像を挿入</span>&nbsp;&nbsp;&nbsp;
		<span class='psudo_button' onClick='popupLink("<%= id %>");'>外部ページ情報を挿入</span>
	</div>
	<form id='form_<%= id %>' method='post' action='<%= action %>'> 
		<%= hidden %>
		<input id='real_content_<%= id %>' type='hidden' name='content'>
		<textarea id='textarea_<%= id %>' cols='50' rows='4' onKeyPress='insertLinkAuto("<%= id %>", event, false);'></textarea>
		<div id='loading_<%= id %>' style='display:none;'>loading...</div>
		<div id='extra_content_<%= id %>' class='extra_content'>
			<div id='extra_links_<%= id %>' class='extra_links'></div>
			<div>
				<div id='extra_images_<%= id %>' class='extra_images'></div>
				<div style='clear:both;'></div>
			</div>
		</div>
		<span id='submit_button' class='psudo_button' onClick='insertLinkAuto("<%= id %>", null, true);'><%= buttonText %></span>
	</form>
</div>
<%= old %>