var alreadyImg = false;
var alreadyLink = false;
var alreadyHref = false;
var cnt = 0;

function popupImg(id){
	if(!alreadyImg){
		alreadyImg = true;
		showPopUpImg(id);
	}
}
function closePopUpImg(){
	jQuery('#imgPopUp').remove();
	alreadyImg = false;
}
function showPopUpImg(id){
	var h = 150;
	var w = 400;
	var t;
	if( jQuery(window).height() > h ){
		t = jQuery(window).scrollTop() + (jQuery(window).height() - h) / 2;
	}else{
		t= jQuery(window).scrollTop() + 20;
	}
	var l;
	if( jQuery(window).width() > w ){
		l = jQuery(window).scrollLeft() + (jQuery(window).width() - w) / 2;
	}else{
		l = jQuery(window).scrollLeft() + 20;
	}
	
	var innerImg = "";
	innerImg += "<div id='imgPopUp' class='popup_img' style='width:"+w+"px; height:"+h+"px;'>";
	innerImg += "<div class='dragHandle'>"
	innerImg += "<span class='popup_close' onClick='closePopUpImg()'>&nbsp;</span>";
	innerImg += "</div>";
	innerImg += "<div class='popup_content'>"
	innerImg += "<input type='file' id='imgfile_"+id+"_"+cnt+"' />";
	innerImg += "<br>";
	innerImg += "<span class='psudo_button' onClick='insertImg(\""+id+"\");'>挿入</span>";
	innerImg += "<div class='notice' style='margin-top:1em;'>※JPEG・PNG・GIF・BMP形式</div>";
	innerImg += "</div>";
	innerImg += "</div>";
	
	jQuery("body").append(innerImg);
	jQuery("#imgPopUp").css("position","absolute");	
	jQuery("#imgPopUp").css("top", t + "px");
	jQuery("#imgPopUp").css("left", l + "px");
	jQuery("#imgPopUp").draggable({handle:"dragHandle"});
	jQuery("#imgPopUp").fadeIn(1000);
}
function insertImg(id){
	var imgSrc = "";
	var fd = new FormData();
	fd.append('file', jQuery("#imgfile_"+id+"_"+cnt)[0].files[0]);

	var ajaxImg = jQuery.ajax({
		url: '/servlet/upload_image',
		data: fd,
		processData: false,
		type: "POST",
		contentType: false,
		success: function(data){
			imgSrc = data;
		},
		beforeSend: function() {
			closePopUpImg();
			showLoading(id);
		}
	});
	
	ajaxImg.complete(function(){
		var insertText = "";
		var imgTag = "";
		if(imgSrc==""){
			imgTag += "<div class='error'>";
			imtTag += "画像をアップロードできませんでした。";
			imgTag += "</div>"
		}else{
			imgTag += "<img src='/servlet/images/"+imgSrc+"'>";
		}
		insertText += "<div id='ex_img_"+id+"_"+cnt+"' class='single'>";
		insertText += imgTag;
		insertText += "<div class='to_be_deleted'>";
		insertText += "<span class='psudo_button'"
						+" onClick='jQuery(\"#ex_img_"+id+"_"+cnt+"\").remove();"
						+" resizePopUp(\""+id+"\");'>削除</span>";
		insertText += "</div>";
		insertText += "</div>";
		cnt ++;
		jQuery("#extra_images_"+id).append(insertText);
		hideLoading(id);
	});
}

function popupLink(id){
	if(!alreadyLink){
		alreadyLink = true;
		showPopUpLink(id);
	}
}
function closePopUpLink(){
	jQuery('#linkPopUp').remove();
	alreadyLink = false;
}
function showPopUpLink(id){
	var h = 110;
	var w = 400;
	var t;
	if( jQuery(window).height() > h ){
		t = jQuery(window).scrollTop() + (jQuery(window).height() - h) / 2;
	}else{
		t= jQuery(window).scrollTop() + 20;
	}
	var l;
	if( jQuery(window).width() > w ){
		l = jQuery(window).scrollLeft() + (jQuery(window).width() - w) / 2;
	}else{
		l = jQuery(window).scrollLeft() + 20;
	}
	
	var innerLink = "";
	innerLink += "<div id='linkPopUp' class='popup_img' style='width:"+w+"px; height:"+h+"px;'>";
	innerLink += "<div class='dragHandle'>"
	innerLink += "<span class='popup_close' onClick='closePopUpLink();'>&nbsp;</span>";
	innerLink += "</div>";
	innerLink += "<div class='popup_content'>"
	innerLink += "<span class='input_label'>URL：</span>";
	innerLink += "<input id='insertLinkUrl' style='width:8em; ime-mode:disabled;'>";
	innerLink += "<br>";
	innerLink += "<span class='psudo_button'"
					+" onClick='insertLink(\""+id+"\",jQuery(\"#insertLinkUrl\").val(), false); "
					+" resizePopUp(\""+id+"\");"
					+" closePopUpLink();'>挿入</span>";
	innerLink += "</div>";
	innerLink += "</div>";
	
	jQuery("body").append(innerLink);
	jQuery("#linkPopUp").css("position","absolute");	
	jQuery("#linkPopUp").css("top", t + "px");
	jQuery("#linkPopUp").css("left", l + "px");
	jQuery("#linkPopUp").draggable({handle:"dragHandle"});
	jQuery("#linkPopUp").fadeIn(1000);
}

function escapeAll(str){
	str = str.replace(/\&/g,"&amp;");
	str = str.replace(/\"/g,"&quot;");
	str = str.replace(/\'/g,"&#039;");
	str = str.replace(/</g,"&lt;");
	str = str.replace(/>/g,"&gt;");
	return str;
}
function chgThm(id,currentCnt,newCurrent){
	jQuery(".t_"+id+"_"+currentCnt).each(function(i){
		if(jQuery(this).attr("id")=="thum_"+id+"_"+currentCnt+"_"+newCurrent){
			jQuery(this).removeClass("cand");
			jQuery(this).addClass("current");
		}else{
			jQuery(this).removeClass("current");
			jQuery(this).addClass("cand");			
		}
	});
}
function insertLink(id, addr, submitFlag){
	showLoading(id);
	jQuery("body").append("<div id='external_html_"+cnt+"'></div>");

	addr = escapeAll(addr);
	addr = addr.replace("https://", "");
	addr = addr.replace("http://", "");
	addr = addr.replace("www.", "");
	addr = "http://www." + addr;
	
	var result = false;
	jQuery("#external_html_"+cnt).load("/servlet/external/"+addr, function(){
		var title = jQuery("#external_html_"+cnt).find("title").html();
		if(title==null || title==""){ title = addr;}
		
		var description = "";
		jQuery("#external_html_"+cnt).find("meta").each(function(i){
			if(jQuery(this).attr("name")!=null 
					&& jQuery(this).attr("name")=="description"){
				description = jQuery(this).attr("content");
			}
		});
		
		var imgs = new Array();
		var imgCnt = 0;
		jQuery("#external_html_"+cnt).find("img").each(function(i){
			if(jQuery.inArray(jQuery(this), imgs)<0){
				imgs[imgCnt] = jQuery(this);
				imgCnt++;
			}
		});
		var insertText = "";
		insertText += "<div id='ex_"+id+"_"+cnt+"' class='single'>";
		insertText += "<table class='page_info'>";
		insertText += "<tr>";
		insertText += "<td id='ex_link_img_"+id+"_"+cnt+"' class='thumb'>";
		if(imgCnt>0){
			insertText += "<img src='"+imgs[0].attr("src")+"'"
								+" width='"+imgs[0].width()+"'"
								+" height='"+imgs[0].height()+"'>";
		}
		insertText += "</td>";
		insertText += "<td>";
		insertText += "<a href='"+addr+"' target='_blank'>"+title+"</a>";
		insertText += "<br>" + addr;
		insertText += "<div class='description'>" + description+"</div>";
		insertText += "</td>";
		insertText += "</tr>";
		insertText += "</table>";
		insertText += "<div class='to_be_deleted'>";
		if(imgCnt>0){
			insertText += "<div class='bold'>サムネイルを変更</div>";
			insertText += "<div style='margin:0.3em;'>";
			insertText += "<span id='thum_"+id+"_"+cnt+"_0'"
							+" class='thum cand t_"+id+"_"+cnt+"'"
							+" onClick='jQuery(\"#ex_link_img_"+id+"_"+cnt+"\")"
							+".html(\"<img src=\\\"\\\" width=\\\"0\\\" height=\\\"0\\\">\");"
							+" chgThm(\""+id+"\",\""+cnt+"\",\"0\");'>";
			insertText += "なし</span>";
			
			for(i=0; i<imgCnt; i++){
				num = i+1;
				c = num==1 ? 'current' : 'cand';
				if(num%15==14){
					insertText += "</div><div style='margin:0.3em;'>";
				}
				insertText += "<span id='thum_"+id+"_"+cnt+"_"+num+"' "
								+ "class='thum "+c+" t_"+id+"_"+cnt+"' "
								+" onClick='jQuery(\"#ex_link_img_"+id+"_"+cnt+"\")"
								+".html(\"<img src=\\\""+imgs[i].attr("src")+"\\\" width=\\\""+imgs[i].width()+"\\\" height=\\\""+imgs[i].height()+"\\\">\");"
								+ "chgThm(\""+id+"\",\""+cnt+"\",\""+num+"\");'>";
				insertText += num + "</span>";
			}
			insertText += "</div>";
		}
		insertText += "<span class='psudo_button'"
						+" onClick='jQuery(\"#ex_"+id+"_"+cnt+"\").remove();"
						+" resizePopUp(\""+id+"\");'>削除</span>";
		insertText +="<div style='clear:both'></div>";
		insertText += "</div>";
		jQuery("#extra_links_"+id).append(insertText);
		resizePopUp(id);
		jQuery("#external_html_"+cnt).remove();
		cnt ++;
		hideLoading(id);
		if(submitFlag){
			putContent(id);
			jQuery("#form_"+id).submit();
		}
	});
}
function insertLinkAuto(id, event, submitFlag){
	jQuery('#submit_button').hide();
	var insertFlag = false;
	if(!alreadyHref){
		var autoFlag = false;
		if(submitFlag){
			autoFlag = true;
		}else{
			if (!event.which && ((event.charCode || event.charCode === 0) ? event.charCode: event.keyCode)) {
			    event.which = event.charCode || event.keyCode;
			}
			if(event.which==13){
				autoFlag = true;
			}
		}
		if( autoFlag ){
			var matched = jQuery("#textarea_"+id).val().match(/(http|www)[\x21-\x7e]+/);
			if(matched!=null){
				insertFlag = true;
				var addr = matched[0];
				alreadyHref = true;
				insertLink(id, addr, submitFlag);
			}
		}
	}
	if(!insertFlag && submitFlag){
		putContent(id);
		jQuery("#form_"+id).submit();		
	}
	jQuery('#submit_button').show();
}

function putContent(id){
	var textContent = jQuery("#textarea_"+id).val();
	textContent = escapeAll(textContent);
	textContent = textContent.replace(/https/gi, "http");
	textContent = textContent.replace(/http:\/\/www\./gi,"http://");
	textContent = textContent.replace(/(http:\/\/|www\.)([\x21-\x7e]+)/gi, "<a href='http://www.$2' target='_blank'>$1$2</a>");
	textContent = textContent.replace(/<a[^>]*>(.*)(gif|jpg|jpeg|jpe|bmp|png|tiff|tif|dib)<\/a>/gi, "<img src='$1$2' class='ext_img'>");
	textContent = textContent.replace(/\n/g,"<br>");
	jQuery('.to_be_deleted').remove();
	jQuery("#clear_"+id).remove();
	var exContent = jQuery("#extra_content_"+id).html();
	exContent = "<div class='extra_content'>" + exContent + "</div>";
	
	var realContent = textContent + exContent;
	jQuery('#real_content_'+id).val(realContent);

	alreadyHref = false;
	cnt = 0;
}

function resizePopUp(id){
	var new_h = jQuery('#textarea_load_'+id).height() + jQuery('#commentDrag').height() + 10;
	jQuery('#commentPopUp').css("height", new_h+"px");	
}
function resizeImg(){
	jQuery('.ext_img').each(function(i){
		var imgW=0, imgH=0;
		if ( typeof this.naturalWidth !== 'undefined' ) {  // for Firefox, Safari, Chrome
			imgW = this.naturalWidth;
			imgH = this.naturalHeight;
		}else{
			imgW = this.width;
			imgH = this.height;
		}
		if(imgH>imgW && imgH>100){
			jQuery(this).height(100);
		}else if(imgH<=imgW && imgW>100){
			jQuery(this).width(100);
		}else if(imgH==0 && imgW==0){
			jQuery(this).height(100);
		}else{
			jQuery(this).width(imgW);
			jQuery(this).width(imgH)
		}
	});
}
function showLoading(id){
	jQuery("#loading_"+id).show();
	resizePopUp(id);
}
function hideLoading(id){
	jQuery("#loading_"+id).hide();
	resizePopUp(id);
}
