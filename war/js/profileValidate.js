function validateWhole(){
	jQuery('.validate').each(function() {
		jQuery(this).validate({
			errorClass: "invalid",
			rules: {
				email: {
					required: true,
					maxlength: 50,
					email: true,
					remote: {
						url:'/servlet/duplication',
						type:'post',
						data: {column:'email'}
					}
				},
				password1: {
					required: true,
					maxlength: 20
				},
				password2: {
					required: true,
					equalTo : '#password1'
				},
				nickName: {
					maxlength: 100,
					remote: {
						url:'/servlet/duplication',
						type:'post',
						data:{column:'nickName'}
					}
				},
				birth_year: {
					digits: true,
					range: [1900, 2100]
				},
				website: {
					maxlength: 100,
					url: true
				}
			},
			messages: {
				email: {
					required: '必須項目です。',
					maxlength: '長すぎます。50字以内で入力してください。',
					email: '不正なアドレスです。',
					remote: '既に登録されています。'
				},
				password1: {
					required: '必須項目です。',
					maxlength: '長すぎます。20字以内で入力してください。',
				},
				password2: '不一致です。',
				nickName:{
					maxlength: '長すぎます。100字以内で入力してください。',
					remote: '既に登録されているため、使えません。'
				},
				birth_year: '西暦で入力してください。',
				website: {
					maxlength: '長すぎます。100字以内で入力してください。',
					url: '不正なURLです。'
				}			
			}
		});
	});
}