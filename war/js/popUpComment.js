var alreadyComment = false;

function popUpComment(postID,commentNum){
	if( !alreadyComment ){
		alreadyComment = true;
		showPopUpComment(postID,commentNum);
	}
}

function closePopUpComment(){
	jQuery("#commentPopUp").remove();
	alreadyComment = false;
}

function showPopUpComment(postID,commentNum){
	var h = 200;
	var w = 600;
	var t;
	if( jQuery(window).height() > h ){
		t = jQuery(window).scrollTop() + (jQuery(window).height() - h) / 2;
	}else{
		t= jQuery(window).scrollTop() + 20;
	}
	var l;
	if( jQuery(window).width() > w ){
		l = jQuery(window).scrollLeft() + (jQuery(window).width() - w) / 2;
	}else{
		l = jQuery(window).scrollLeft() + 20;
	}
	
	var inner = "";
	var popupID = "c_"+postID+"_"+commentNum;
	inner += "<div id='commentPopUp' class='popup_comment' style='width:"+w+"px; height:"+h+"px;'>";
	inner += "<div id='commentDrag' class='dragHandle'>"
	inner += "<span class='popup_close' onClick='closePopUpComment()'>&nbsp;</span>";
	inner += "</div>";
	inner += "<div id='textarea_load_"+popupID+"' class='popup_content'></div>";
	inner += "</div>";
	
	jQuery("body").append(inner);
	jQuery("#commentPopUp").css("position","absolute");	
	jQuery("#commentPopUp").css("top", t + "px");
	jQuery("#commentPopUp").css("left", l + "px");
	jQuery("#commentPopUp").draggable({handle:"dragHandle"});
	jQuery("#textarea_load_"+popupID).load("/load/textarea/"+popupID);
	jQuery("#commentPopUp").fadeIn(1000);
}
