var alreadyPrefecture = false;

function popUpPrefecture(){
	if( !alreadyPrefecture ){
		alreadyPrefecture = true;
		jQuery("#home").hide();
		var dummyOption = jQuery('#home option:selected').val();
		if(dummyOption==null){
			dummyOption = "都道府県をお選びください。";
		}
		var dummyInner;
		dummyInner = "<select id='dummy' disabled>";
		dummyInner += "<option>"+dummyOption+"</option>";
		dummyInner += "</select>";
		jQuery("#dummy_home").append(dummyInner);
		showPopUpPrefecture();
	}
}

function closePopUpPrefecture(){
	jQuery("#popUpPrefecture").remove();
	jQuery("#dummy").remove();
	jQuery("#home").show();
	alreadyPrefecture = false;
}

function showPopUpPrefecture(){
	var h = 300;
	var w = 600;
	var t = jQuery("#dummy_home").position().top + jQuery("#dummy_home").height() + 5;
	var l;
	if( jQuery(window).width() > w ){
		l = jQuery(window).scrollLeft() + (jQuery(window).width() - w) / 2;
	}else{
		l = jQuery(window).scrollLeft() + 20;
	}
	
	var str = "";
	str += "<div id='popUpPrefecture' class='popup_prefecture' style='width:"+w+"px; height:"+h+"px;'>";
	str += "<div class='dragHandle'>"
	str += "<span class='popup_close' onClick='closePopUpPrefecture();'>&nbsp;</span>";
	str += "</div>";
	str += "<table id='prefectures'>";
	str += "<tr>";
	str += "<th>北海道・東北</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"北海道\"); closePopUpPrefecture();'>北海道</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"青森県\"); closePopUpPrefecture();'>青森県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"秋田県\"); closePopUpPrefecture();'>秋田県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"岩手県\"); closePopUpPrefecture();'>岩手県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"山形県\"); closePopUpPrefecture();'>山形県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"宮城県\"); closePopUpPrefecture();'>宮城県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"福島県\"); closePopUpPrefecture();'>福島県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>甲信越・北陸</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"山梨県\"); closePopUpPrefecture();'>山梨県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"長野県\"); closePopUpPrefecture();'>長野県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"新潟県\"); closePopUpPrefecture();'>新潟県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"富山県\"); closePopUpPrefecture();'>富山県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"石川県\"); closePopUpPrefecture();'>石川県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"福井県\"); closePopUpPrefecture();'>福井県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>関東</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"茨城県\"); closePopUpPrefecture();'>茨城県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"栃木県\"); closePopUpPrefecture();'>栃木県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"群馬県\"); closePopUpPrefecture();'>群馬県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"埼玉県\"); closePopUpPrefecture();'>埼玉県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"千葉県\"); closePopUpPrefecture();'>千葉県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"東京都\"); closePopUpPrefecture();'>東京都</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"神奈川県\"); closePopUpPrefecture();'>神奈川県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>東海</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"愛知県\"); closePopUpPrefecture();'>愛知県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"静岡県\"); closePopUpPrefecture();'>静岡県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"岐阜県\"); closePopUpPrefecture();'>岐阜県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"三重県\"); closePopUpPrefecture();'>三重県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>関西</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"大阪府\"); closePopUpPrefecture();'>大阪府</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"兵庫県\"); closePopUpPrefecture();'>兵庫県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"京都府\"); closePopUpPrefecture();'>京都府</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"滋賀県\"); closePopUpPrefecture();'>滋賀県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"奈良県\"); closePopUpPrefecture();'>奈良県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"和歌山県\"); closePopUpPrefecture();'>和歌山県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>中国</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"岡山県\"); closePopUpPrefecture();'>岡山県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"広島県\"); closePopUpPrefecture();'>広島県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"鳥取県\"); closePopUpPrefecture();'>鳥取県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"島根県\"); closePopUpPrefecture();'>島根県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"山口県\"); closePopUpPrefecture();'>山口県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>四国</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"徳島県\"); closePopUpPrefecture();'>徳島県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"香川県\"); closePopUpPrefecture();'>香川県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"愛媛県\"); closePopUpPrefecture();'>愛媛県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"高知県\"); closePopUpPrefecture();'>高知県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>九州・沖縄</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"福岡県\"); closePopUpPrefecture();'>福岡県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"佐賀県\"); closePopUpPrefecture();'>佐賀県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"長崎県\"); closePopUpPrefecture();'>長崎県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"熊本県\"); closePopUpPrefecture();'>熊本県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"大分県\"); closePopUpPrefecture();'>大分県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"宮崎県\"); closePopUpPrefecture();'>宮崎県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"鹿児島県\"); closePopUpPrefecture();'>鹿児島県</span>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"沖縄県\"); closePopUpPrefecture();'>沖縄県</span>";
	str += "</td>";
	str += "</tr>";
	str += "<tr>";
	str += "<th>その他</th>";
	str += "<td class='separator'>｜</td>";
	str += "<td>";
	str += "<span class='pref_item' onClick='jQuery(\"#home\").val(\"その他\"); closePopUpPrefecture();'>その他</span>";
	str += "</td>";
	str += "</tr>";
	str += "</table>";
	str += "</div>";
	
	jQuery("body").append(str);
	jQuery("#popUpPrefecture").css("position","absolute");	
	jQuery("#popUpPrefecture").css("top", t + "px");
	jQuery("#popUpPrefecture").css("left", l + "px");
	jQuery("#popUpPrefecture").draggable({handle:"dragHandle"});
	jQuery("#popUpPrefecture").slideDown("fast");
}
