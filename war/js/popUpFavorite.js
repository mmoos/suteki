var alreadyFavorite = new Array(0);

function popUpFavorite(postID){
	var flagFavorite = true;
	for(i=0; i<alreadyFavorite.length; i++){
		if(alreadyFavorite[i]==postID){
			flagFavorite = false;
			break;
		}
	}
	
	if( flagFavorite ){
		alreadyFavorite.push(postID);
		showFavorite(postID);
	}
}

function closeFavorite(postID){
	jQuery("#popular_"+postID).remove();
	var copyAlreadyFavorite = new Array(alreadyFavorite.length - 1);
	var cnt = 0;
	for(i=0; i<alreadyFavorite.length; i++){
		if(alreadyFavorite[i]!=postID){
			copyAlreadyFavorite[cnt] = alreadyFavorite[i];
		}
		cnt ++;
	}
	alreadyFavorite = copyAlreadyFavorite;

}

function showFavorite(postID){
	var h = 400;
	var w = 400;
	var t;
	if( jQuery(window).height() > h ){
		t = jQuery(window).scrollTop() + (jQuery(window).height() - h) / 2;
	}else{
		t= jQuery(window).scrollTop() + 20;
	}
	var l;
	if( jQuery(window).width() > w ){
		l = jQuery(window).scrollLeft() + (jQuery(window).width() - w) / 2;
	}else{
		l = jQuery(window).scrollLeft() + 20;
	}

	var inner = "";
	inner += "<div id='popular_"+postID+"' class='popup_favorite' style='width:"+w+"px; height:"+h+"px;'>";
	inner += "<div class='dragHandle'>共感している人"
	inner += "<span class='popup_close' onClick='closeFavorite(\""+postID+"\")'>&nbsp;</span>";
	inner += "</div>"
	inner += "<div id='members_"+postID+"' class='popup_content'>&nbsp;</div>";
	inner += "</div>";
	
	jQuery("body").append(inner);
	jQuery("#popular_"+postID).css("position","absolute");
	jQuery("#popular_"+postID).css("top", t + "px");
	jQuery("#popular_"+postID).css("left", l + "px");
	jQuery("#popular_"+postID).draggable({handle:"dragHandle"});
	
	jQuery("#members_"+postID).load("/load/members/"+postID);
	jQuery("#members_"+postID).css("position","absolute");
	jQuery("#members_"+postID).css("top", "30px");
	
	jQuery("#popular_"+postID).fadeIn(1500);
}

